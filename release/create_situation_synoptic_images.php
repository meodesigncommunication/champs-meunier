<?php

// execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/gefiswiss/champs-meunier/release/create_situation_synoptic_images.php


require_once dirname(__FILE__) . "/../site/wp/wp-content/plugins/meo-real-estate-developments/classes/svglib/svglib.php";

define('IMAGE_WIDTH',  392);
define('IMAGE_HEIGHT', 155);

define('COLOUR_NORMAL',     '#E5E5E5');
define('COLOUR_HIGHLIGHT',  '#2a5828');

define('SOURCE_DIR', 'E:/Development/gefiswiss/champs-meunier/release/create_situation_synoptic_images/source/');
define('DEST_DIR',   'E:/Development/gefiswiss/champs-meunier/release/create_situation_synoptic_images/out/');


// Layer names in the SVG files
$svg_ids = array(
	'situationSynoptique_champsMeunier_rez.svg' => array(
		'A11',
		'A211',
		'A321',
		'A12',
		'A212',
		'A322',
		'B131',
		'B132',
		'B241',
		'B242',
		'B351',
		'B352'
	),
	'situationSynoptique_champsMeunier_1er.svg' => array(
		'A13',
		'A14',
		'A15',
		'A213',
		'A214',
		'A215',
		'A323',
		'A324',
		'A325',
		'B133',
		'B134',
		'B135',
		'B243',
		'B244',
		'B245',
		'B353',
		'B354',
		'B355'
	),
	'situationSynoptique_champsMeunier_2eme.svg' => array(
		'A16',
		'A17',
		'A18',
		'A216',
		'A217',
		'A218',
		'A326',
		'A327',
		'A328',
		'B136',
		'B137',
		'B138',
		'B246',
		'B247',
		'B248',
		'B356',
		'B357',
		'B358'
	),
	'situationSynoptique_champsMeunier_attique.svg' => array(
		'A110',
		'A19',
		'A219',
		'A220',
		'A329',
		'A330',
		'B139',
		'B140',
		'B249',
		'B250',
		'B359',
		'B360'
	)
);



function svg_to_file($svg, $name) {
	$fh = fopen($name, 'w') or die("can't open file: " . $name);
	fwrite($fh,  $svg->asXML(null, false));
	fclose($fh);
}

function svg_to_png($svg, $name) {
	$xml = $svg->asXML(null, false);

	$imageMagick = new Imagick();
	$imageMagick->setBackgroundColor(new ImagickPixel('transparent'));
	$imageMagick->readImageBlob($xml);
	$imageMagick->setImageFormat("png24");
	$imageMagick->resizeImage(IMAGE_WIDTH, IMAGE_HEIGHT, imagick::FILTER_LANCZOS, 1);

	$imageMagick->writeImage($name);

	$imageMagick->clear();
	$imageMagick->destroy();
}


foreach ($svg_ids as $file => $layers) {
	echo "Processing " . $file . "\n";
	$svg = SVGDocument::getInstance( SOURCE_DIR . $file );

	foreach ($layers as $layer) {
		$lot_element = $svg->getElementById( $layer );
		$old_fill = $lot_element->getAttribute("fill");
		$lot_element->setAttribute("fill",   COLOUR_HIGHLIGHT);

		$filename = preg_replace('/_1_$/', '', $layer);
		echo "\t" . $filename . "\n";

		svg_to_file($svg, DEST_DIR . "svg/lot-situation-" . $filename . ".svg");
		svg_to_png($svg,  DEST_DIR . "png/lot-situation-" . $filename . ".png");

		$lot_element->setAttribute("fill", $old_fill);

	}
}
