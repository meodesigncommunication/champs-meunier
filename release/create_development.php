<?php

// To execute on the server, copy into the /wp folder and access directly

// To execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/gefiswiss/champs-meunier/release/create_development.php


if (php_sapi_name() == "cli") {
	$_SERVER['SERVER_ADDR'] = "127.0.0.1"; // force WP to use local config
	define('MEO_WP_ROOT', dirname(__FILE__) . '/../site/wp');
}
else {
	header("Content-Type: text/plain");
	define('MEO_WP_ROOT', dirname(__FILE__));
}

include_once(MEO_WP_ROOT . '/wp-load.php');

global $wpdb;

require_once( MEO_WP_ROOT . '/wp-content/plugins/meo-real-estate-developments/classes/class-development-dao-p2p.php');
$dao = new DevelopmentDaoP2P();

function summarise_field_data($field_definitions) {
	$result = array();

	foreach($field_definitions as $field_definition) {
		$result[$field_definition['name']] = array(
			'type' => $field_definition['type'],
			'key'  => $field_definition['key']
		);
		if ($field_definition['type'] == "repeater") {
			$result[$field_definition['name']]['sub_fields'] = summarise_field_data($field_definition['sub_fields']);
		}
	}

	return $result;
}

function get_image_id($name) {
	global $wpdb;

	$sql = "select a.id
	          from wp_posts a
	         where a.post_type = 'attachment'
	           and a.guid like %s ";

	$sql = $wpdb->prepare($sql, '%/' . $name);

	$image_id = $wpdb->get_var($sql);

	return $image_id;
}

function get_missing_images($objects, $fields) {
	$result = array();

	foreach ($objects as $object_id => $object_data) {
		if (!array_key_exists('meta', $object_data)) {
			continue;
		}
		foreach ($object_data['meta'] as $name => $value) {
			if ($fields[$name]['type'] == "image" || $fields[$name]['type'] == "file") {
				$image_id = get_image_id($value);
				if (empty($image_id)) {
					$result[] = $value;
				}
			}
			elseif ($fields[$name]['type'] == "repeater") {
				// TODO: handle images in repeaters
			}
		}
	}

	return $result;
}

function flatten_qtrans_field($value) {
	if (!is_array($value)) {
		return $value;
	}

	// Flatten array into mqTranslate format string (assumes array with key of language code)
	$result = '';
	foreach ($value as $lang => $value) {
		$result .= '<!--:' . $lang . '-->' . $value . '<!--:-->';
	}

	return $result;
}

function create_objects($type, $data, $fields, &$all_objects) {
	global $wpdb;

	$object_sql = "select p.id
	          from $wpdb->posts p
	         where p.post_type = %s
	           and p.post_title = %s
	           and p.post_name = %s
	           and p.post_status = 'publish'";

	$meta_sql = "insert into $wpdb->postmeta (post_id, meta_key, meta_value ) values (%d, %s, %s) ";

	$link_sql = "insert into {$wpdb->prefix}p2p ( p2p_from, p2p_to, p2p_type ) values ( %d, %d, %s ) ";

	$date = date('Y-m-d 10:00:00');

	foreach ($data as $object_id => $object_details) {
		$name = flatten_qtrans_field($object_details['base']['name']);

		$object_id_db = $wpdb->get_var(
			$wpdb->prepare($object_sql,
			               $type,
			               $name,
			               $object_details['base']['slug'])
		);

		$content = $object_details['base']['content'] ? $object_details['base']['content'] : array( 'fr' => '&nbsp;', 'en' => '&nbsp;', 'de' => '&nbsp;' );
		$content = flatten_qtrans_field($content);

		if (empty($object_id_db)) {
			// Post doesn't exist

			$object_id_db = wp_insert_post( array(
				'post_title'     => $name,
				'post_name'      => $object_details['base']['slug'],
				'post_content'   => $content,
				'post_status'    => 'publish',
				'post_type'      => $type,
				'post_author'    => 1,
				'ping_status'    => 'closed',
				'post_date'      => $date,
				'post_date_gmt'  => $date,
				'comment_status' => 'closed'
			) );

			if (isset($object_details['meta'])) {
				foreach ($object_details['meta'] as $fieldname => $value) {
					$field = $fields[$fieldname];
					if ($field['type'] == "image" || $field['type'] == "file") {
						$value = get_image_id($value);
					}
					elseif ($field['type'] == "qtranslate_text" && is_array($value)) {
						$value = flatten_qtrans_field($value);
					}

					$meta_insert = $wpdb->prepare(
						$meta_sql,
						$object_id_db,
						$fieldname,
						$value
					);

					$wpdb->query( $meta_insert );

					$meta_insert = $wpdb->prepare(
						$meta_sql,
						$object_id_db,
						'_' . $fieldname,
						$field['key']
					);

					$wpdb->query( $meta_insert );
				}
			}

			if (isset($object_details['links'])) {
				foreach ($object_details['links'] as $link) {

					$target_id = $all_objects[$link['link_target']]['data'][$link['target_id']]['id'];

					$link_insert = $wpdb->prepare(
						$link_sql,
						$object_id_db,
						$target_id,
						$link['link_type']
					);

					$wpdb->query( $link_insert );
				}
			}
		}

		$all_objects[$type]['data'][$object_id]['id'] = $object_id_db;
	}
}

// ---------------------------------------------------------------------------------------------------
// Data
// ---------------------------------------------------------------------------------------------------

// For image fields, use the file name.
// It's assumed to be unique, and already uploaded to WordPress.
// This is checked before data insertion

$developments = array(
	'1' => array(
		'base' => array(
			'name' => 'Champs Meunier',
			'slug' => 'champs-meunier'
		),
		'meta' => array(
			'code' => 'Champs Meunier',
			'plan' => 'TODO_development.jpg',
			'plan_hover_transparent' => 'TODO_development_hover.png'
		)
	)
);

$sectors = array(
	'1' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Champs Meunier Secteur 1',
				'en' => 'Champs Meunier Sector 1',
				'de' => 'Champs Meunier Sektor 1'
			),
			'slug' => 'sector-1'
		),
		'meta' => array(
			'code' => 'Sector 1'
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_SECTOR_DEV,
				'link_target' => DevelopmentDaoP2P::CPT_DEVELOPMENT,
				'target_id' => '1'
			)
		)
	)
);

$buildings = array(
	'1' => array(
		'base' => array(
			'name' => "les Blés",
			'slug' => 'les-bles'
		),
		'meta' => array(
			'code' => 'A1',
			'building_image' => 'facade_ChampsMeunier_Bats-A1-A2-A3.png',
			'top_floor_offset' => '16.55',
			'css_floor_height' => '18.25',
			// 'dev_plan_coordinates' => '',
			// 'dev_plan_hover_image' => ''
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_BUILDING_SECTOR,
				'link_target' => DevelopmentDaoP2P::CPT_SECTOR,
				'target_id' => '1'
			)
		)
	),
	'2' => array(
		'base' => array(
			'name' => "l'Avoine",
			'slug' => 'avoine'
		),
		'meta' => array(
			'code' => 'A2',
			'building_image' => 'facade_ChampsMeunier_Bats-A1-A2-A3.png',
			'top_floor_offset' => '16.55',
			'css_floor_height' => '18.25',
			// 'dev_plan_coordinates' => '',
			// 'dev_plan_hover_image' => ''
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_BUILDING_SECTOR,
				'link_target' => DevelopmentDaoP2P::CPT_SECTOR,
				'target_id' => '1'
			)
		)
	),
	'3' => array(
		'base' => array(
			'name' => "le Seigle",
			'slug' => 'le-seigle'
		),
		'meta' => array(
			'code' => 'A3',
			'building_image' => 'facade_ChampsMeunier_Bats-A1-A2-A3.png',
			'top_floor_offset' => '16.55',
			'css_floor_height' => '18.25',
			// 'dev_plan_coordinates' => '',
			// 'dev_plan_hover_image' => ''
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_BUILDING_SECTOR,
				'link_target' => DevelopmentDaoP2P::CPT_SECTOR,
				'target_id' => '1'
			)
		)
	),

	'4' => array(
		'base' => array(
			'name' => "l'Epautre",
			'slug' => 'epautre'
		),
		'meta' => array(
			'code' => 'B1',
			'building_image' => 'facade_ChampsMeunier_Bats-B1-B2-B3.png',
			'top_floor_offset' => '8.7',
			'css_floor_height' => '17.5',
			// 'dev_plan_coordinates' => '',
			// 'dev_plan_hover_image' => ''
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_BUILDING_SECTOR,
				'link_target' => DevelopmentDaoP2P::CPT_SECTOR,
				'target_id' => '1'
			)
		)
	),

	'5' => array(
		'base' => array(
			'name' => "le Sésame",
			'slug' => 'le-sesame'
		),
		'meta' => array(
			'code' => 'B2',
			'building_image' => 'facade_ChampsMeunier_Bats-B1-B2-B3.png',
			'top_floor_offset' => '8.7',
			'css_floor_height' => '17.5',
			// 'dev_plan_coordinates' => '',
			// 'dev_plan_hover_image' => ''
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_BUILDING_SECTOR,
				'link_target' => DevelopmentDaoP2P::CPT_SECTOR,
				'target_id' => '1'
			)
		)
	),

	'6' => array(
		'base' => array(
			'name' => "le Millet",
			'slug' => 'le-millet'
		),
		'meta' => array(
			'code' => 'B3',
			'building_image' => 'facade_ChampsMeunier_Bats-B1-B2-B3.png',
			'top_floor_offset' => '8.7',
			'css_floor_height' => '17.5',
			// 'dev_plan_coordinates' => '',
			// 'dev_plan_hover_image' => ''
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_BUILDING_SECTOR,
				'link_target' => DevelopmentDaoP2P::CPT_SECTOR,
				'target_id' => '1'
			)
		)
	)
);

//
//

$floors = array(
	'1' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez inférieur',
				'en' => 'Lower ground',
				'de' => 'Unteres'
			),
			'slug' => 'les-bles-rez-inferieur'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'rez inf',
				'en' => 'LG',
				'de' => 'U'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'rez inf',
				'en' => 'LG',
				'de' => 'U'
			), // name with ordinal eg 1er
			'order' => '0',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '1'
			)
		)
	),
	'2' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez supérieur',
				'en' => 'Upper ground',
				'de' => 'höhere Ebene'
			),
			'slug' => 'les-bles-rez-superieur'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'rez sup',
				'en' => 'UG',
				'de' => 'E'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'rez sup',
				'en' => 'UG',
				'de' => 'E'
			), // name with ordinal eg 1er
			'order' => '1',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_1er.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_1er-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_1er-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '1'
			)
		)
	),
	'3' => array(
		'base' => array(
			'name' => array(
				'fr' => '1er étage',
				'en' => '1st floor',
				'de' => '1. Stock'
			),
			'slug' => 'les-bles-1er-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '1',
				'en' => '1',
				'de' => '1'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '1<sup>er</sup>',
				'en' => '1<sup>st</sup>',
				'de' => '1'
			), // name with ordinal eg 1er
			'order' => '2',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '1'
			)
		)
	),
	'4' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Penthouse'
			),
			'slug' => 'les-bles-attique'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			), // name with ordinal eg 1er
			'order' => '3',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '1'
			)
		)
	),
	'5' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez inférieur',
				'en' => 'Lower ground',
				'de' => 'Unteres'
			),
			'slug' => 'avoine-rez-inferieur'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'rez inf',
				'en' => 'LG',
				'de' => 'U'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'rez inf',
				'en' => 'LG',
				'de' => 'U'
			), // name with ordinal eg 1er
			'order' => '0',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '2'
			)
		)
	),
	'6' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez supérieur',
				'en' => 'Upper ground',
				'de' => 'höhere Ebene'
			),
			'slug' => 'avoine-rez-superieur'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'rez sup',
				'en' => 'UG',
				'de' => 'E'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'rez sup',
				'en' => 'UG',
				'de' => 'E'
			), // name with ordinal eg 1er
			'order' => '1',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_1er.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_1er-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_1er-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '2'
			)
		)
	),
	'7' => array(
		'base' => array(
			'name' => array(
				'fr' => '1er étage',
				'en' => '1st floor',
				'de' => '1. Stock'
			),
			'slug' => 'avoine-1er-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '1',
				'en' => '1',
				'de' => '1'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '1<sup>er</sup>',
				'en' => '1<sup>st</sup>',
				'de' => '1'
			), // name with ordinal eg 1er
			'order' => '2',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '2'
			)
		)
	),
	'8' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Penthouse'
			),
			'slug' => 'avoine-attique'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			), // name with ordinal eg 1er
			'order' => '3',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '2'
			)
		)
	),
	'9' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez inférieur',
				'en' => 'Lower ground',
				'de' => 'Unteres'
			),
			'slug' => 'le-seigle-rez-inferieur'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'rez inf',
				'en' => 'LG',
				'de' => 'U'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'rez inf',
				'en' => 'LG',
				'de' => 'U'
			), // name with ordinal eg 1er
			'order' => '0',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Rez-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '3'
			)
		)
	),
	'10' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez supérieur',
				'en' => 'Upper ground',
				'de' => 'höhere Ebene'
			),
			'slug' => 'le-seigle-rez-superieur'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'rez sup',
				'en' => 'UG',
				'de' => 'E'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'rez sup',
				'en' => 'UG',
				'de' => 'E'
			), // name with ordinal eg 1er
			'order' => '1',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_1er.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_1er-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_1er-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '3'
			)
		)
	),
	'11' => array(
		'base' => array(
			'name' => array(
				'fr' => '1er étage',
				'en' => '1st floor',
				'de' => '1. Stock'
			),
			'slug' => 'le-seigle-1er-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '1',
				'en' => '1',
				'de' => '1'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '1<sup>er</sup>',
				'en' => '1<sup>st</sup>',
				'de' => '1'
			), // name with ordinal eg 1er
			'order' => '2',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_2eme-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '3'
			)
		)
	),
	'12' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Penthouse'
			),
			'slug' => 'le-seigle-attique'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			), // name with ordinal eg 1er
			'order' => '3',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_A1-A2-A3_Attique-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '3'
			)
		)
	),
	'13' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez de chaussée',
				'en' => 'Ground floor',
				'de' => 'Erdgeschoss'
			),
			'slug' => 'epautre-rez-de-chaussee'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'RDC',
				'en' => 'G',
				'de' => 'E'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Rez',
				'en' => 'G',
				'de' => 'E'
			), // name with ordinal eg 1er
			'order' => '0',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '4'
			)
		)
	),
	'14' => array(
		'base' => array(
			'name' => array(
				'fr' => '1er étage',
				'en' => '1st floor',
				'de' => '1. Stock'
			),
			'slug' => 'epautre-1er-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '1',
				'en' => '1',
				'de' => '1'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '1<sup>er</sup>',
				'en' => '1<sup>st</sup>',
				'de' => '1'
			), // name with ordinal eg 1er
			'order' => '1',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_1er.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_1er-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_1er-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '4'
			)
		)
	),
	'15' => array(
		'base' => array(
			'name' => array(
				'fr' => '2ème étage',
				'en' => '2nd floor',
				'de' => '2. Stock'
			),
			'slug' => 'epautre-2eme-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '2',
				'en' => '2',
				'de' => '2'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '2<sup>ème</sup>',
				'en' => '2<sup>nd</sup>',
				'de' => '2'
			), // name with ordinal eg 1er
			'order' => '2',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '4'
			)
		)
	),
	'16' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Penthouse'
			),
			'slug' => 'epautre-attique'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			), // name with ordinal eg 1er
			'order' => '3',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '4'
			)
		)
	),
	'17' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez de chaussée',
				'en' => 'Ground floor',
				'de' => 'Erdgeschoss'
			),
			'slug' => 'le-sesame-rez-de-chaussee'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'RDC',
				'en' => 'G',
				'de' => 'E'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Rez',
				'en' => 'G',
				'de' => 'E'
			), // name with ordinal eg 1er
			'order' => '0',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '5'
			)
		)
	),
	'18' => array(
		'base' => array(
			'name' => array(
				'fr' => '1er étage',
				'en' => '1st floor',
				'de' => '1. Stock'
			),
			'slug' => 'le-sesame-1er-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '1',
				'en' => '1',
				'de' => '1'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '1<sup>er</sup>',
				'en' => '1<sup>st</sup>',
				'de' => '1'
			), // name with ordinal eg 1er
			'order' => '1',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_1er.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_1er-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_1er-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '5'
			)
		)
	),
	'19' => array(
		'base' => array(
			'name' => array(
				'fr' => '2ème étage',
				'en' => '2nd floor',
				'de' => '2. Stock'
			),
			'slug' => 'le-sesame-2eme-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '2',
				'en' => '2',
				'de' => '2'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '2<sup>ème</sup>',
				'en' => '2<sup>nd</sup>',
				'de' => '2'
			), // name with ordinal eg 1er
			'order' => '2',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '5'
			)
		)
	),
	'20' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Penthouse'
			),
			'slug' => 'le-sesame-attique'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			), // name with ordinal eg 1er
			'order' => '3',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '5'
			)
		)
	),
	'21' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Rez de chaussée',
				'en' => 'Ground floor',
				'de' => 'Erdgeschoss'
			),
			'slug' => 'le-millet-rez-de-chaussee'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'RDC',
				'en' => 'G',
				'de' => 'E'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Rez',
				'en' => 'G',
				'de' => 'E'
			), // name with ordinal eg 1er
			'order' => '0',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Rez-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '6'
			)
		)
	),
	'22' => array(
		'base' => array(
			'name' => array(
				'fr' => '1er étage',
				'en' => '1st floor',
				'de' => '1. Stock'
			),
			'slug' => 'le-millet-1er-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '1',
				'en' => '1',
				'de' => '1'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '1<sup>er</sup>',
				'en' => '1<sup>st</sup>',
				'de' => '1'
			), // name with ordinal eg 1er
			'order' => '1',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_1er.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_1er-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_1er-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '6'
			)
		)
	),
	'23' => array(
		'base' => array(
			'name' => array(
				'fr' => '2ème étage',
				'en' => '2nd floor',
				'de' => '2. Stock'
			),
			'slug' => 'le-millet-2eme-etage'
		),
		'meta' => array(
			'code' => array(
				'fr' => '2',
				'en' => '2',
				'de' => '2'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => '2<sup>ème</sup>',
				'en' => '2<sup>nd</sup>',
				'de' => '2'
			), // name with ordinal eg 1er
			'order' => '2',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_2eme-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '6'
			)
		)
	),
	'24' => array(
		'base' => array(
			'name' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Penthouse'
			),
			'slug' => 'le-millet-attique'
		),
		'meta' => array(
			'code' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			),    // short name, eg 1
			'ordinal' => array(
				'fr' => 'Attique',
				'en' => 'Attic',
				'de' => 'Pent.'
			), // name with ordinal eg 1er
			'order' => '3',   // within the building, 0 = lowest floor
			'plan' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique.svg',    // SVG plan of floor
			'walls_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique-floor.png',   // PNG with only the walls not transparent
			'floor_overlay_image' => 'KeyPlan_champsMeunier_B1-B2-B3_Attique-floor-overlay.png',   // transparent image the same size as walls_image
			// 'dev_plan_coordinates' => '', // series of x,y cooordinates for the development image map
			// 'dev_plan_hover_image' => '', // hover image for the development image map
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_FLOOR_BUILDING,
				'link_target' => DevelopmentDaoP2P::CPT_BUILDING,
				'target_id' => '6'
			)
		)
	)
);

$plans = array(
	/* --------------------------------A - 1, 11, 21 -------------------------------- */
	'1' => array(
		'base' => array(
			'name' => 'A - 1, 11, 21',
			'slug' => 'a-1-11-21'
		),
		'meta' => array(
			'code' => 'A - 1, 11, 21',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-1-11-21.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-1-11-21.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'L1-L11-L21_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 2, 12, 22 -------------------------------- */
	'2' => array(
		'base' => array(
			'name' => 'A - 2, 12, 22',
			'slug' => 'a-2-12-22'
		),
		'meta' => array(
			'code' => 'A - 2, 12, 22',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-2-12-22.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-2-12-22.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'L2-L12-L22_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 3, 13, 23 -------------------------------- */
	'3' => array(
		'base' => array(
			'name' => 'A - 3, 13, 23',
			'slug' => 'a-3-13-23'
		),
		'meta' => array(
			'code' => 'A - 3, 13, 23',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-3-6-9-13-16-19-23-26-29.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-3-6-9-13-16-19-23-26-29.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L3_x5F_A2-L13_x5F_A3-L23_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 4, 14, 24 -------------------------------- */
	'4' => array(
		'base' => array(
			'name' => 'A - 4, 14, 24',
			'slug' => 'a-4-14-24'
		),
		'meta' => array(
			'code' => 'A - 4, 14, 24',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-4-7-14-17-24-27.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-4-7-14-17-24-27.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L4_x5F_A2-L14_x5F_A3-L24_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 5, 15, 25 -------------------------------- */
	'5' => array(
		'base' => array(
			'name' => 'A - 5, 15, 25',
			'slug' => 'a-5-15-25'
		),
		'meta' => array(
			'code' => 'A - 5, 15, 25',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-5-15-25.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-5-15-25.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L5_x5F_A2-L15_x5F_A3-L25_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 6, 16, 26 -------------------------------- */
	'6' => array(
		'base' => array(
			'name' => 'A - 6, 16, 26',
			'slug' => 'a-6-16-26'
		),
		'meta' => array(
			'code' => 'A - 6, 16, 26',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-3-6-9-13-16-19-23-26-29.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-3-6-9-13-16-19-23-26-29.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L6_x5F_A2-L16_x5F_A3-L26_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 7, 17, 27 -------------------------------- */
	'7' => array(
		'base' => array(
			'name' => 'A - 7, 17, 27',
			'slug' => 'a-7-17-27'
		),
		'meta' => array(
			'code' => 'A - 7, 17, 27',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-4-7-14-17-24-27.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-4-7-14-17-24-27.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L7_x5F_A2-L17_x5F_A3-L27_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 8, 18, 28 -------------------------------- */
	'8' => array(
		'base' => array(
			'name' => 'A - 8, 18, 28',
			'slug' => 'a-8-18-28'
		),
		'meta' => array(
			'code' => 'A - 8, 18, 28',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-8-18-28.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-8-18-28.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L8_x5F_A2-L18_x5F_A3-L28_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 9, 19, 29 -------------------------------- */
	'9' => array(
		'base' => array(
			'name' => 'A - 9, 19, 29',
			'slug' => 'a-9-19-29'
		),
		'meta' => array(
			'code' => 'A - 9, 19, 29',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-3-6-9-13-16-19-23-26-29.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-3-6-9-13-16-19-23-26-29.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L9_x5F_A2-L19_x5F_A3-L29_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------A - 10, 20, 30 -------------------------------- */
	'10' => array(
		'base' => array(
			'name' => 'A - 10, 20, 30',
			'slug' => 'a-10-20-30'
		),
		'meta' => array(
			'code' => 'A - 10, 20, 30',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-10-20-30.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-10-20-30.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'A1-L10_x5F_A2-L20_x5F_A3-L30_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 31, 41, 51 -------------------------------- */
	'11' => array(
		'base' => array(
			'name' => 'B - 31, 41, 51',
			'slug' => 'b-31-41-51'
		),
		'meta' => array(
			'code' => 'B - 31, 41, 51',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-31-41-51.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-31-41-51.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L31_x5F_B2-L41_x5F_B3-L51_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 32, 42, 52 -------------------------------- */
	'12' => array(
		'base' => array(
			'name' => 'B - 32, 42, 52',
			'slug' => 'b-32-42-52'
		),
		'meta' => array(
			'code' => 'B - 32, 42, 52',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-32-42-52.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-32-42-52.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L32_x5F_B2-L42_x5F_B3-L52_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 33, 43,  53 -------------------------------- */
	'13' => array(
		'base' => array(
			'name' => 'B - 33, 43,  53',
			'slug' => 'b-33-43-53'
		),
		'meta' => array(
			'code' => 'B - 33, 43,  53',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-33-36-43-46-53-56.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-33-36-43-46-53-56.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L33_x5F_B2-L43_x5F_B3-L53_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 34, 44, 54 -------------------------------- */
	'14' => array(
		'base' => array(
			'name' => 'B - 34, 44, 54',
			'slug' => 'b-34-44-54'
		),
		'meta' => array(
			'code' => 'B - 34, 44, 54',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-34-37-44-47-54-57.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-34-37-44-47-54-57.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L34_x5F_B2-L44_x5F_B3-L54_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 35, 45, 55 -------------------------------- */
	'15' => array(
		'base' => array(
			'name' => 'B - 35, 45, 55',
			'slug' => 'b-35-45-55'
		),
		'meta' => array(
			'code' => 'B - 35, 45, 55',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-35-45-55.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-35-45-55.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L35_x5F_B2-L45_x5F_B3-L55_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 36, 46, 56 -------------------------------- */
	'16' => array(
		'base' => array(
			'name' => 'B - 36, 46, 56',
			'slug' => 'b-36-46-56'
		),
		'meta' => array(
			'code' => 'B - 36, 46, 56',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-33-36-43-46-53-56.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-33-36-43-46-53-56.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L36_x5F_B2-L46_x5F_B3-L56_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 37, 47, 57 -------------------------------- */
	'17' => array(
		'base' => array(
			'name' => 'B - 37, 47, 57',
			'slug' => 'b-37-47-57'
		),
		'meta' => array(
			'code' => 'B - 37, 47, 57',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-34-37-44-47-54-57.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-34-37-44-47-54-57.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L37_x5F_B2-L47_x5F_B3-L57_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 38, 48, 58 -------------------------------- */
	'18' => array(
		'base' => array(
			'name' => 'B - 38, 48, 58',
			'slug' => 'b-38-48-58'
		),
		'meta' => array(
			'code' => 'B - 38, 48, 58',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-38-40-48-50-58-60.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-38-40-48-50-58-60.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L38_x5F_B2-L48_x5F_B3-L58_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 39, 49, 59 -------------------------------- */
	'19' => array(
		'base' => array(
			'name' => 'B - 39, 49, 59',
			'slug' => 'b-39-49-59'
		),
		'meta' => array(
			'code' => 'B - 39, 49, 59',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-39-49-59.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-39-49-59.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L39_x5F_B2-L49_x5F_B3-L59_1_', // Layer in the floor SVG
		)
	),
	/* --------------------------------B - 40, 50, 60 -------------------------------- */
	'20' => array(
		'base' => array(
			'name' => 'B - 40, 50, 60',
			'slug' => 'b-40-50-60'
		),
		'meta' => array(
			'code' => 'B - 40, 50, 60',
			'detailed_plan_image' => 'planAppartement_Champs-Meunier_LOTS-38-40-48-50-58-60.svg', // large plan image (SVG)
			'detailed_plan_image_fallback' => 'planAppartement_Champs-Meunier_LOTS-38-40-48-50-58-60.png', // large plan image (for browsers that don't support SVG)
			'pdf' => 'TODO_plan.pdf', // Plan PDF
			'svg_id' => 'B1-L40_x5F_B2-L50_x5F_B3-L60_1_', // Layer in the floor SVG
		)
	)
);

$lots = array(
	'1' => array(
		'base' => array(
			'name' => 'Lot 1',
			'name' => 'lot-1'
		),
		'meta' => array(
			'code' => 'Lot 1',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '49',
			// 'surface_garden' => '',
			'surface_weighted' => '83.17',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '1'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '1'
			)
		)
	),
	'2' => array(
		'base' => array(
			'name' => 'Lot 2',
			'name' => 'lot-2'
		),
		'meta' => array(
			'code' => 'Lot 2',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '67',
			// 'surface_garden' => '',
			'surface_weighted' => '84.2',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '1'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '2'
			)
		)
	),
	'3' => array(
		'base' => array(
			'name' => 'Lot 3',
			'name' => 'lot-3'
		),
		'meta' => array(
			'code' => 'Lot 3',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.63',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '2'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '3'
			)
		)
	),
	'4' => array(
		'base' => array(
			'name' => 'Lot 4',
			'name' => 'lot-4'
		),
		'meta' => array(
			'code' => 'Lot 4',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '72.52',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '2'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '4'
			)
		)
	),
	'5' => array(
		'base' => array(
			'name' => 'Lot 5',
			'name' => 'lot-5'
		),
		'meta' => array(
			'code' => 'Lot 5',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '110.77',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '2'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '5'
			)
		)
	),
	'6' => array(
		'base' => array(
			'name' => 'Lot 6',
			'name' => 'lot-6'
		),
		'meta' => array(
			'code' => 'Lot 6',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.61',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '3'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '6'
			)
		)
	),
	'7' => array(
		'base' => array(
			'name' => 'Lot 7',
			'name' => 'lot-7'
		),
		'meta' => array(
			'code' => 'Lot 7',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '72.59',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '3'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '7'
			)
		)
	),
	'8' => array(
		'base' => array(
			'name' => 'Lot 8',
			'name' => 'lot-8'
		),
		'meta' => array(
			'code' => 'Lot 8',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '129.57',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '3'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '8'
			)
		)
	),
	'9' => array(
		'base' => array(
			'name' => 'Lot 9',
			'name' => 'lot-9'
		),
		'meta' => array(
			'code' => 'Lot 9',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.78',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '4'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '9'
			)
		)
	),
	'10' => array(
		'base' => array(
			'name' => 'Lot 10',
			'name' => 'lot-10'
		),
		'meta' => array(
			'code' => 'Lot 10',
			// 'opus_code' => '',
			'pieces' => '5.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '183.72',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '4'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '10'
			)
		)
	),
	'11' => array(
		'base' => array(
			'name' => 'Lot 11',
			'name' => 'lot-11'
		),
		'meta' => array(
			'code' => 'Lot 11',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '49',
			// 'surface_garden' => '',
			'surface_weighted' => '83.17',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '5'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '1'
			)
		)
	),
	'12' => array(
		'base' => array(
			'name' => 'Lot 12',
			'name' => 'lot-12'
		),
		'meta' => array(
			'code' => 'Lot 12',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '67',
			// 'surface_garden' => '',
			'surface_weighted' => '84.28',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '5'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '2'
			)
		)
	),
	'13' => array(
		'base' => array(
			'name' => 'Lot 13',
			'name' => 'lot-13'
		),
		'meta' => array(
			'code' => 'Lot 13',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.72',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '6'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '3'
			)
		)
	),
	'14' => array(
		'base' => array(
			'name' => 'Lot 14',
			'name' => 'lot-14'
		),
		'meta' => array(
			'code' => 'Lot 14',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '72.52',
			'availability' => 'reserved',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '6'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '4'
			)
		)
	),
	'15' => array(
		'base' => array(
			'name' => 'Lot 15',
			'name' => 'lot-15'
		),
		'meta' => array(
			'code' => 'Lot 15',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '110.77',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '6'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '5'
			)
		)
	),
	'16' => array(
		'base' => array(
			'name' => 'Lot 16',
			'name' => 'lot-16'
		),
		'meta' => array(
			'code' => 'Lot 16',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.61',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '7'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '6'
			)
		)
	),
	'17' => array(
		'base' => array(
			'name' => 'Lot 17',
			'name' => 'lot-17'
		),
		'meta' => array(
			'code' => 'Lot 17',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '72.63',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '7'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '7'
			)
		)
	),
	'18' => array(
		'base' => array(
			'name' => 'Lot 18',
			'name' => 'lot-18'
		),
		'meta' => array(
			'code' => 'Lot 18',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '129.68',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '7'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '8'
			)
		)
	),
	'19' => array(
		'base' => array(
			'name' => 'Lot 19',
			'name' => 'lot-19'
		),
		'meta' => array(
			'code' => 'Lot 19',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.89',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '8'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '9'
			)
		)
	),
	'20' => array(
		'base' => array(
			'name' => 'Lot 20',
			'name' => 'lot-20'
		),
		'meta' => array(
			'code' => 'Lot 20',
			// 'opus_code' => '',
			'pieces' => '5.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '183.74',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '8'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '10'
			)
		)
	),
	'21' => array(
		'base' => array(
			'name' => 'Lot 21',
			'name' => 'lot-21'
		),
		'meta' => array(
			'code' => 'Lot 21',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '49',
			// 'surface_garden' => '',
			'surface_weighted' => '83.17',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '9'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '1'
			)
		)
	),
	'22' => array(
		'base' => array(
			'name' => 'Lot 22',
			'name' => 'lot-22'
		),
		'meta' => array(
			'code' => 'Lot 22',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '67',
			// 'surface_garden' => '',
			'surface_weighted' => '84.2',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '9'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '2'
			)
		)
	),
	'23' => array(
		'base' => array(
			'name' => 'Lot 23',
			'name' => 'lot-23'
		),
		'meta' => array(
			'code' => 'Lot 23',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.63',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '10'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '3'
			)
		)
	),
	'24' => array(
		'base' => array(
			'name' => 'Lot 24',
			'name' => 'lot-24'
		),
		'meta' => array(
			'code' => 'Lot 24',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '72.52',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '10'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '4'
			)
		)
	),
	'25' => array(
		'base' => array(
			'name' => 'Lot 25',
			'name' => 'lot-25'
		),
		'meta' => array(
			'code' => 'Lot 25',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '110.77',
			'availability' => 'reserved',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '10'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '5'
			)
		)
	),
	'26' => array(
		'base' => array(
			'name' => 'Lot 26',
			'name' => 'lot-26'
		),
		'meta' => array(
			'code' => 'Lot 26',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.61',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '11'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '6'
			)
		)
	),
	'27' => array(
		'base' => array(
			'name' => 'Lot 27',
			'name' => 'lot-27'
		),
		'meta' => array(
			'code' => 'Lot 27',
			// 'opus_code' => '',
			'pieces' => '2.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '72.59',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '11'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '7'
			)
		)
	),
	'28' => array(
		'base' => array(
			'name' => 'Lot 28',
			'name' => 'lot-28'
		),
		'meta' => array(
			'code' => 'Lot 28',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '129.57',
			'availability' => 'reserved',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '11'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '8'
			)
		)
	),
	'29' => array(
		'base' => array(
			'name' => 'Lot 29',
			'name' => 'lot-29'
		),
		'meta' => array(
			'code' => 'Lot 29',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '101.78',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '12'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '9'
			)
		)
	),
	'30' => array(
		'base' => array(
			'name' => 'Lot 30',
			'name' => 'lot-30'
		),
		'meta' => array(
			'code' => 'Lot 30',
			// 'opus_code' => '',
			'pieces' => '5.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '183.72',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '12'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '10'
			)
		)
	),
	'31' => array(
		'base' => array(
			'name' => 'Lot 31',
			'name' => 'lot-31'
		),
		'meta' => array(
			'code' => 'Lot 31',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '47.13',
			// 'surface_garden' => '',
			'surface_weighted' => '132.67',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '13'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '11'
			)
		)
	),
	'32' => array(
		'base' => array(
			'name' => 'Lot 32',
			'name' => 'lot-32'
		),
		'meta' => array(
			'code' => 'Lot 32',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '168',
			// 'surface_garden' => '',
			'surface_weighted' => '124.76',
			'availability' => 'reserved',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '13'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '12'
			)
		)
	),
	'33' => array(
		'base' => array(
			'name' => 'Lot 33',
			'name' => 'lot-33'
		),
		'meta' => array(
			'code' => 'Lot 33',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '132.75',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '14'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '13'
			)
		)
	),
	'34' => array(
		'base' => array(
			'name' => 'Lot 34',
			'name' => 'lot-34'
		),
		'meta' => array(
			'code' => 'Lot 34',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '96.09',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '14'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '14'
			)
		)
	),
	'35' => array(
		'base' => array(
			'name' => 'Lot 35',
			'name' => 'lot-35'
		),
		'meta' => array(
			'code' => 'Lot 35',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.38',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '14'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '15'
			)
		)
	),
	'36' => array(
		'base' => array(
			'name' => 'Lot 36',
			'name' => 'lot-36'
		),
		'meta' => array(
			'code' => 'Lot 36',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '132.89',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '15'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '16'
			)
		)
	),
	'37' => array(
		'base' => array(
			'name' => 'Lot 37',
			'name' => 'lot-37'
		),
		'meta' => array(
			'code' => 'Lot 37',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '96.26',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '15'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '17'
			)
		)
	),
	'38' => array(
		'base' => array(
			'name' => 'Lot 38',
			'name' => 'lot-38'
		),
		'meta' => array(
			'code' => 'Lot 38',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.52',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '15'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '18'
			)
		)
	),
	'39' => array(
		'base' => array(
			'name' => 'Lot 39',
			'name' => 'lot-39'
		),
		'meta' => array(
			'code' => 'Lot 39',
			// 'opus_code' => '',
			'pieces' => '5.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '173.78',
			'availability' => 'reserved',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '16'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '19'
			)
		)
	),
	'40' => array(
		'base' => array(
			'name' => 'Lot 40',
			'name' => 'lot-40'
		),
		'meta' => array(
			'code' => 'Lot 40',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.68',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '16'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '20'
			)
		)
	),
	'41' => array(
		'base' => array(
			'name' => 'Lot 41',
			'name' => 'lot-41'
		),
		'meta' => array(
			'code' => 'Lot 41',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '47.13',
			// 'surface_garden' => '',
			'surface_weighted' => '132.67',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '17'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '11'
			)
		)
	),
	'42' => array(
		'base' => array(
			'name' => 'Lot 42',
			'name' => 'lot-42'
		),
		'meta' => array(
			'code' => 'Lot 42',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '168',
			// 'surface_garden' => '',
			'surface_weighted' => '124.76',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '17'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '12'
			)
		)
	),
	'43' => array(
		'base' => array(
			'name' => 'Lot 43',
			'name' => 'lot-43'
		),
		'meta' => array(
			'code' => 'Lot 43',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '132.75',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '18'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '13'
			)
		)
	),
	'44' => array(
		'base' => array(
			'name' => 'Lot 44',
			'name' => 'lot-44'
		),
		'meta' => array(
			'code' => 'Lot 44',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '96.09',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '18'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '14'
			)
		)
	),
	'45' => array(
		'base' => array(
			'name' => 'Lot 45',
			'name' => 'lot-45'
		),
		'meta' => array(
			'code' => 'Lot 45',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.38',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '18'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '15'
			)
		)
	),
	'46' => array(
		'base' => array(
			'name' => 'Lot 46',
			'name' => 'lot-46'
		),
		'meta' => array(
			'code' => 'Lot 46',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '132.89',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '19'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '16'
			)
		)
	),
	'47' => array(
		'base' => array(
			'name' => 'Lot 47',
			'name' => 'lot-47'
		),
		'meta' => array(
			'code' => 'Lot 47',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '96.26',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '19'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '17'
			)
		)
	),
	'48' => array(
		'base' => array(
			'name' => 'Lot 48',
			'name' => 'lot-48'
		),
		'meta' => array(
			'code' => 'Lot 48',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.52',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '19'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '18'
			)
		)
	),
	'49' => array(
		'base' => array(
			'name' => 'Lot 49',
			'name' => 'lot-49'
		),
		'meta' => array(
			'code' => 'Lot 49',
			// 'opus_code' => '',
			'pieces' => '5.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '173.78',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '20'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '19'
			)
		)
	),
	'50' => array(
		'base' => array(
			'name' => 'Lot 50',
			'name' => 'lot-50'
		),
		'meta' => array(
			'code' => 'Lot 50',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.68',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '20'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '20'
			)
		)
	),
	'51' => array(
		'base' => array(
			'name' => 'Lot 51',
			'name' => 'lot-51'
		),
		'meta' => array(
			'code' => 'Lot 51',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '47.13',
			// 'surface_garden' => '',
			'surface_weighted' => '132.67',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '21'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '11'
			)
		)
	),
	'52' => array(
		'base' => array(
			'name' => 'Lot 52',
			'name' => 'lot-52'
		),
		'meta' => array(
			'code' => 'Lot 52',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '168',
			// 'surface_garden' => '',
			'surface_weighted' => '124.76',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '21'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '12'
			)
		)
	),
	'53' => array(
		'base' => array(
			'name' => 'Lot 53',
			'name' => 'lot-53'
		),
		'meta' => array(
			'code' => 'Lot 53',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '132.75',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '22'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '13'
			)
		)
	),
	'54' => array(
		'base' => array(
			'name' => 'Lot 54',
			'name' => 'lot-54'
		),
		'meta' => array(
			'code' => 'Lot 54',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '96.09',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '22'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '14'
			)
		)
	),
	'55' => array(
		'base' => array(
			'name' => 'Lot 55',
			'name' => 'lot-55'
		),
		'meta' => array(
			'code' => 'Lot 55',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.38',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '22'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '15'
			)
		)
	),
	'56' => array(
		'base' => array(
			'name' => 'Lot 56',
			'name' => 'lot-56'
		),
		'meta' => array(
			'code' => 'Lot 56',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '132.89',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '23'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '16'
			)
		)
	),
	'57' => array(
		'base' => array(
			'name' => 'Lot 57',
			'name' => 'lot-57'
		),
		'meta' => array(
			'code' => 'Lot 57',
			// 'opus_code' => '',
			'pieces' => '3.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '96.26',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '23'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '17'
			)
		)
	),
	'58' => array(
		'base' => array(
			'name' => 'Lot 58',
			'name' => 'lot-58'
		),
		'meta' => array(
			'code' => 'Lot 58',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.52',
			'availability' => 'available',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '23'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '18'
			)
		)
	),
	'59' => array(
		'base' => array(
			'name' => 'Lot 59',
			'name' => 'lot-59'
		),
		'meta' => array(
			'code' => 'Lot 59',
			// 'opus_code' => '',
			'pieces' => '5.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '173.78',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '24'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '19'
			)
		)
	),
	'60' => array(
		'base' => array(
			'name' => 'Lot 60',
			'name' => 'lot-60'
		),
		'meta' => array(
			'code' => 'Lot 60',
			// 'opus_code' => '',
			'pieces' => '4.5',
			// 'surface_interior' => '',
			// 'surface_balcony' => '',
			'surface_terrace' => '',
			// 'surface_garden' => '',
			'surface_weighted' => '128.68',
			'availability' => 'sold',
			// 'svg_id' => '', // Override the plan's SVG ID (layer in the floor SVG)
			// 'pdf' => '', // Override the plan's PDF
			// 'floor_position_image' => '', // SVG file showing the position of the apartment in the building
			// 'floor_position_image_fallback' => '', PNG / JPG  file showing the position of the apartment in the building for browsers that can't handle SVG.  If not present, the floor_position_image file name will be used, changing its extension from SVG to PNG
		),
		'links' => array(
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_FLOOR,
				'link_target' => DevelopmentDaoP2P::CPT_FLOOR,
				'target_id' => '24'
			),
			array(
				'link_type' => DevelopmentDaoP2P::REL_LOT_PLAN,
				'link_target' => DevelopmentDaoP2P::CPT_PLAN,
				'target_id' => '20'
			)
		)
	)
);

$entries = array();


// ---------------------------------------------------------------------------------------------------
// Processing
// ---------------------------------------------------------------------------------------------------

$all_objects = array(
	DevelopmentDaoP2P::CPT_DEVELOPMENT => array(
		'data' => $developments,
		'fields' => summarise_field_data($dao->getDevelopmentFieldDefinitions())
	),
	DevelopmentDaoP2P::CPT_SECTOR => array(
		'data' => $sectors,
		'fields' => summarise_field_data($dao->getSectorFieldDefinitions())
	),
	DevelopmentDaoP2P::CPT_BUILDING => array(
		'data' => $buildings,
		'fields' => summarise_field_data($dao->getBuildingFieldDefinitions())
	),
	DevelopmentDaoP2P::CPT_FLOOR => array(
		'data' => $floors,
		'fields' => summarise_field_data($dao->getFloorFieldDefinitions())
	),
	DevelopmentDaoP2P::CPT_PLAN => array(
		'data' => $plans,
		'fields' => summarise_field_data($dao->getPlanFieldDefinitions())
	),
	DevelopmentDaoP2P::CPT_LOT => array(
		'data' => $lots,
		'fields' => summarise_field_data($dao->getLotFieldDefinitions())
	),
	DevelopmentDaoP2P::CPT_ENTRY => array(
		'data' => $entries,
		'fields' => summarise_field_data($dao->getEntryFieldDefinitions())
	)
);

$missing_images = array();
$some_images_missing = false;

foreach ($all_objects as $type => $data) {
	$missing_images[$type] = get_missing_images($data['data'], $data['fields']);
	$some_images_missing = $some_images_missing || !empty($missing_images[$type]);
}

if ($some_images_missing) {
	echo "Images missing:\n";
	foreach ($missing_images as $type => $images) {
		echo "  $type\n";
		foreach ($images as $image) {
			echo "    $image\n";
		}
		echo "\n";
	}
	exit;
}

echo "All images present\n";

foreach ($all_objects as $type => $data) {
	echo "Creating $type elements\n";
	create_objects($type, $data['data'], $data['fields'], $all_objects);
}
