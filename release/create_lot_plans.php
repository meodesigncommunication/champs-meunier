<?php

// execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/gefiswiss/champs-meunier/release/create_lot_plans.php


require_once dirname(__FILE__) . "/../site/wp/wp-content/plugins/meo-real-estate-developments/classes/svglib/svglib.php";

define('IMAGE_WIDTH',  1260);
define('IMAGE_HEIGHT', 1260);

define('SOURCE_DIR', 'E:/Development/gefiswiss/champs-meunier/release/create_lot_plans/svg/');
define('DEST_DIR',   'E:/Development/gefiswiss/champs-meunier/release/create_lot_plans/png/');


function svg_to_png($svg, $name) {
	$xml = $svg->asXML(null, false);

	$imageMagick = new Imagick();
	$imageMagick->setBackgroundColor(new ImagickPixel('transparent'));
	$imageMagick->readImageBlob($xml);
	$imageMagick->setImageFormat("png24");
	$imageMagick->resizeImage(IMAGE_WIDTH, IMAGE_HEIGHT, imagick::FILTER_LANCZOS, 1);

	$imageMagick->writeImage($name);

	$imageMagick->clear();
	$imageMagick->destroy();
}




$files = scandir(SOURCE_DIR);
foreach ($files as $filename) {
	if (substr($filename, -3) !== "svg") {
		continue;
	}
	echo $filename . "\n";
	$png_file = preg_replace('/\.svg$/', '.png', $filename);

	$svg = SVGDocument::getInstance( SOURCE_DIR . $filename );
	svg_to_png($svg, DEST_DIR . $png_file);

}
