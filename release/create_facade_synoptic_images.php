<?php

// execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/gefiswiss/champs-meunier/release/create_facade_synoptic_images.php


require_once dirname(__FILE__) . "/../site/wp/wp-content/plugins/meo-real-estate-developments/classes/svglib/svglib.php";

define('IMAGE_WIDTH',  780);
define('IMAGE_HEIGHT', 350);

define('COLOUR_NORMAL',     '#E5E5E5');
define('COLOUR_HIGHLIGHT',  '#2a5828');

define('SOURCE_DIR', 'E:/Development/gefiswiss/champs-meunier/release/create_facade_synoptic_images/source/');
define('DEST_DIR',   'E:/Development/gefiswiss/champs-meunier/release/create_facade_synoptic_images/out/');


// Layer names in the SVG files
$svg_ids = array(
	'facade_synoptique_ChampsMeunier_Bats-A1-A2-A3.svg' => array(
		'A11-A211-A321',
		'A12-A212-A322',
		'A13-A213-A323',
		'A14-A214-A324',
		'A15-A215-A325',
		'A16-A216-A326',
		'A17-A217-A327',
		'A18-A218-A328',
		'A19-A219-A329',
		'A110-A220-A330'
	),
	'facade_synoptique_ChampsMeunier_Bats-B1-B2-B3.svg' => array(
		'B131-B241-B351_1_',
		'B132-B242-B352_1_',
		'B133-B243-B353_1_',
		'B134-B244-B354_1_',
		'B135-B245-B355_1_',
		'B136-B246-B356_1_',
		'B137-B247-B357_1_',
		'B138-B248-B358_1_',
		'B139-B249-B359_1_',
		'B140-B250-B360_1_'
	)
);



function svg_to_file($svg, $name) {
	$fh = fopen($name, 'w') or die("can't open file: " . $name);
	fwrite($fh,  $svg->asXML(null, false));
	fclose($fh);
}

function svg_to_png($svg, $name) {
	$xml = $svg->asXML(null, false);

	$imageMagick = new Imagick();
	$imageMagick->setBackgroundColor(new ImagickPixel('transparent'));
	$imageMagick->readImageBlob($xml);
	$imageMagick->setImageFormat("png24");
	$imageMagick->resizeImage(IMAGE_WIDTH, IMAGE_HEIGHT, imagick::FILTER_LANCZOS, 1);

	$imageMagick->writeImage($name);

	$imageMagick->clear();
	$imageMagick->destroy();
}


foreach ($svg_ids as $file => $layers) {
	echo "Processing " . $file . "\n";
	$svg = SVGDocument::getInstance( SOURCE_DIR . $file );

	// Make sure all apartments are not highlighted before we start
	foreach ($layers as $layer) {
		$lot_element = $svg->getElementById( $layer );
		$lot_element->setAttribute("fill",   COLOUR_NORMAL);
	}

	foreach ($layers as $layer) {
		$lot_element = $svg->getElementById( $layer );
		$lot_element->setAttribute("fill",   COLOUR_HIGHLIGHT);

		$filename = preg_replace('/_1_$/', '', $layer);
		echo "\t" . $filename . "\n";

		svg_to_file($svg, DEST_DIR . "svg/lot-facade-" . $filename . ".svg");
		svg_to_png($svg,  DEST_DIR . "png/lot-facade-" . $filename . ".png");

		$lot_element->setAttribute("fill",   COLOUR_NORMAL);

	}
}
