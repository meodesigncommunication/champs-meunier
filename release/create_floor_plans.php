<?php

// execute from the command line:
// php --php-ini d:/conf/php/php.ini E:/Development/gefiswiss/champs-meunier/release/create_floor_plans.php

// Creates floor outlines (walls) and lot hover images



require_once dirname(__FILE__) . "/../site/wp/wp-content/plugins/meo-real-estate-developments/classes/svglib/svglib.php";
require_once dirname(__FILE__) . "/../site/wp/wp-content/plugins/meo-real-estate-developments/shortcodes.php";

define('FLOOR_WIDTH',  1900);
define('FLOOR_HEIGHT',  575);

define('COLOUR_AVAILABLE_HOVER', '#005025');
define('COLOUR_AVAILABLE',       '#E2E2E2');
define('COLOUR_RESERVED_HOVER',  '#FF8A00');
define('COLOUR_RESERVED',        '#FFAB37');
define('COLOUR_SOLD_HOVER',      '#FF0000');
define('COLOUR_SOLD',            '#FF4E28');

define('SOURCE_DIRECTORY', 'E:/Development/gefiswiss/champs-meunier/release/create_floor_plans/svg/'); // Include trailing slash
define('PNG_DIRECTORY',    'E:/Development/gefiswiss/champs-meunier/release/create_floor_plans/png/');
define('LABEL_CSS_FILE',   'E:/Development/gefiswiss/champs-meunier/release/create_floor_plans/labels.css');

define('LOT_BASE_URL', 'http://local.champs-meunier.ch/lot/');

global $colours;
$colours = array(
	'available-hover' => COLOUR_AVAILABLE_HOVER,
	'available'       => COLOUR_AVAILABLE,
	'reserved-hover'  => COLOUR_RESERVED_HOVER,
	'reserved'        => COLOUR_RESERVED,
	'sold-hover'      => COLOUR_SOLD_HOVER,
	'sold'            => COLOUR_SOLD
);



function mred_dump_image($floor_plan, $name) {
	$xml = $floor_plan->asXML(null, false);

	$imageMagick = new Imagick();
	$imageMagick->setBackgroundColor(new ImagickPixel('transparent'));
	$imageMagick->readImageBlob($xml);
	$imageMagick->setImageFormat("png24");
	$imageMagick->resizeImage(FLOOR_WIDTH, FLOOR_HEIGHT, imagick::FILTER_LANCZOS, 1);

	$imageMagick->writeImage(PNG_DIRECTORY . $name);

	$imageMagick->clear();
	$imageMagick->destroy();
}

function mred_dump_lot($floor_plan, $plan_element, $colour, $filename) {
	$plan_element->setAttribute('fill', $colour);
	mred_dump_image($floor_plan, $filename);
}

function mred_create_floor_pngs($floor_plan, $filename) {
	global $colours;

	$basename = basename($filename, ".svg");

	echo "Processing " . $basename . "\n";

	// Floor plan (just walls)
	$hover_layer = $floor_plan->getElementById( 'HOVER' );
	$hover_layer->setAttribute("display", "none");

	$xml = $floor_plan->asXML(null, false);
	mred_dump_image($floor_plan, $basename . "-floor.png");



	// Hide eveything
	foreach ( $floor_plan->children() as $child ) {
		$child->setAttribute("display", "none");
	}
	foreach ($hover_layer->children() as $child) {
		$child->setAttribute("display", "none");
	}


	mred_dump_image($floor_plan, $basename . "-floor-overlay.png");

	// Show hover layer
	$hover_layer->removeAttribute('display');

	$fh = fopen(LABEL_CSS_FILE, 'a') or die("can't open file");

	foreach ($hover_layer->children() as $child) {
		$child->setAttribute("display", "none");
		$id = $child->getAttribute( 'id');
		if (empty($id)) {
			continue;
		}
		echo "\t"  .$id . "\n";
		$child->removeAttribute("display"); // display layer

		foreach ($colours as $label => $colour) {
			mred_dump_lot($floor_plan, $child, $colour, 'lots/' . $id . "-" . $label . ".png");
		}

		$child->setAttribute("display", "none"); // hide layer again


		$points = mred_get_svg_element_points($child);
		$extent = mred_get_points_extent($points);
		$width = $extent['max_x'] - $extent['min_x'];
		$height = $extent['max_y'] - $extent['min_y'];

		$left_pct = round($extent['min_x'] / FLOOR_WIDTH * 100, 4);
		$width_pct = round($width / FLOOR_WIDTH * 100, 4);
		$top_pct =  round(($extent['min_y'] + ($height / 2)) / FLOOR_WIDTH * 100, 4); // Use FLOOR_WIDTH because paddings are % of width


		fwrite($fh, '.label_' . strtolower($id) . ' {margin-top: ' . $top_pct . '%; left: ' . $left_pct . '%; width: ' . $width_pct . '%;}' . "\n");
	}

	fclose($fh);

}

/* ------------------------------------------------------------------------------------------ */

$files = scandir(SOURCE_DIRECTORY);

if (file_exists(LABEL_CSS_FILE)) { unlink (LABEL_CSS_FILE); }

foreach ($files as $filename) {
	if (substr($filename, -3) !== "svg") {
		continue;
	}

	$svg = SVGDocument::getInstance( SOURCE_DIRECTORY . $filename );
	mred_create_floor_pngs($svg, $filename);
}
