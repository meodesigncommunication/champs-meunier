<?php
class MredTablePDF extends FPDF
{
	//variables of html parser
	var $B;
	var $I;
	var $U;
	var $HREF;
	var $fontList;
	var $issetfont;
	var $issetcolor;
	
	var $development_ID;
	
	function MredTablePDF($orientation='P', $unit='mm', $format='A4')
	{
	    //Call parent constructor
	    $this->FPDF($orientation,$unit,$format);
	    //Initialization
	    $this->B=0;
	    $this->I=0;
	    $this->U=0;
	    $this->HREF='';
	
	    $this->tableborder=0;
	    $this->tdbegin=false;
	    $this->tdwidth=0;
	    $this->tdheight=0;
	    $this->tdalign="C";
	    $this->tdbgcolor=false;
	
	    $this->oldx=0;
	    $this->oldy=0;
	
	    $this->fontlist=array("arial","times","courier","helvetica","symbol");
	    $this->issetfont=false;
	    $this->issetcolor=false;
	    
	    // Development ID
	    $this->development_ID = 816; // Set your own
	    // TODO :  make it flexible for several developments
	}
	
	function Header()
	{
	    $this->Image(plugin_dir_path( __FILE__ ). 'images/logo.jpg',65,5,80);
	    $this->SetXY(10, 40);
	    
	    // Headers
		$HTMLheaders = '
			<table border="0">
				<tr>
					<td width="770" height="80" bgcolor="#389143">Liste de prix / '.date('d-m-y H:i').'</td>
				</tr>
				<tr>
					<td width="110" height="80" bgcolor="#2a5828">R�F�RENCE</td>
					<td width="110" height="80" bgcolor="#2a5828">B�TIMENT</td>
					<td width="110" height="80" bgcolor="#2a5828">NBR PCS</td>
					<td width="110" height="80" bgcolor="#2a5828">�TAGE</td>
					<td width="110" height="80" bgcolor="#2a5828">PARKING</td>
					<td width="110" height="80" bgcolor="#2a5828">SURF</td>
					<td width="110" height="80" bgcolor="#2a5828">PRIX / STATUS</td>
				</tr>
			</table>';
		
		// Headers
		$this->SetFont('Helvetica','',11);
		$this->SetTextColor(255, 255, 255);
		$this->WriteHTML($HTMLheaders);
	}
	
	
	function Footer()
	{
		// Get development info
		$exclude = array('plan_hover_transparent', 'plan', 'code', 'url', 'slug', 'name');
		$dev_info = mred_get_development($this->development_ID, $exclude);
		
	    // Positionnement � 1,5 cm du bas
	    $this->SetY(-40);
	    // Police Arial italique 8
	    $this->SetFont('Helvetica','',11);
	    $this->SetTextColor(255, 255, 255);
	    
	    // Main left
	    $this->MultiCell(80, 30, "Garage", 0, "C", "#939598");
	    
	    // Tops rights
	    $this->SetXY(90, -40);
		$this->MultiCell(57, 15, "place de parc voiture", 0, "C", "#939598");
		$this->SetXY(147, -40);
		$this->MultiCell(56, 15, get_field('place_de_parc_voiture', $dev_info['id']), 0, "C", "#939598");
		
		// Bottoms right
		$this->SetXY(90, -25);
		$this->MultiCell(57, 15, "place de parc moto", 0, "C", "#939598");
		$this->SetXY(147, -25);
		$this->MultiCell(56, 15, get_field('place_de_parc_moto', $dev_info['id']), 0, "C", "#939598");
		
	    // Num�ro de page centr�
	    //$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	}
	
	//////////////////////////////////////
	//html parser
	
	function WriteHTML($html)
	{
		$html = $this->unhtmlentities($html);
	    $html=strip_tags($html,"<b><u><i><a><img><p><br><strong><em><font><tr><blockquote><hr><td><tr><table><sup>"); //remove all unsupported tags
	    $html=str_replace("\n",'',$html); //replace carriage returns by spaces
	    $html=str_replace("\t",'',$html); //replace carriage returns by spaces
	    $a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE); //explodes the string
	    foreach($a as $i=>$e)
	    {
	        if($i%2==0)
	        {
	            //Text
	            if($this->HREF)
	                $this->PutLink($this->HREF,$e);
	            elseif($this->tdbegin) {
	                if(trim($e)!='' && $e!="&nbsp;") {
	                    $this->Cell($this->tdwidth,$this->tdheight,$e,$this->tableborder,'',$this->tdalign,$this->tdbgcolor);
	                }
	                elseif($e=="&nbsp;") {
	                    $this->Cell($this->tdwidth,$this->tdheight,'',$this->tableborder,'',$this->tdalign,$this->tdbgcolor);
	                }
	            }
	            else
	                $this->Write(5,stripslashes(txtentities($e)));
	        }
	        else
	        {
	            //Tag
	            if($e[0]=='/')
	                $this->CloseTag(strtoupper(substr($e,1)));
	            else
	            {
	                //Extract attributes
	                $a2=explode(' ',$e);
	                $tag=strtoupper(array_shift($a2));
	                $attr=array();
	                foreach($a2 as $v)
	                {
	                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
	                        $attr[strtoupper($a3[1])]=$a3[2];
	                }
	                $this->OpenTag($tag,$attr);
	            }
	        }
	    }
	}
	
	function OpenTag($tag, $attr)
	{
	    //Opening tag
	    switch($tag){
	
	        case 'SUP':
	            if( !empty($attr['SUP']) ) {    
	                //Set current font to 6pt     
	                $this->SetFont('','',6);
	                //Start 125cm plus width of cell to the right of left margin         
	                //Superscript "1" 
	                $this->Cell(2,2,$attr['SUP'],0,0,'L');
	            }
	            break;
	
	        case 'TABLE': // TABLE-BEGIN
	            if( !empty($attr['BORDER']) ) $this->tableborder=$attr['BORDER'];
	            else $this->tableborder=0;
	            break;
	        case 'TR': //TR-BEGIN
	            break;
	        case 'TD': // TD-BEGIN
	            if( !empty($attr['WIDTH']) ) $this->tdwidth=($attr['WIDTH']/4);
	            else $this->tdwidth=40; // Set to your own width if you need bigger fixed cells
	            if( !empty($attr['HEIGHT']) ) $this->tdheight=($attr['HEIGHT']/6);
	            else $this->tdheight=6; // Set to your own height if you need bigger fixed cells
	            if( !empty($attr['ALIGN']) ) {
	                $align=$attr['ALIGN'];        
	                if($align=='LEFT') $this->tdalign='L';
	                if($align=='CENTER') $this->tdalign='C';
	                if($align=='RIGHT') $this->tdalign='R';
	            }
	            else $this->tdalign='C'; // Set to your own
	            if( !empty($attr['BGCOLOR']) ) {
	                $coul=hex2dec($attr['BGCOLOR']);
	                    $this->SetFillColor($coul['R'],$coul['G'],$coul['B']);
	                    $this->tdbgcolor=true;
	                }
	            $this->tdbegin=true;
	            break;
	
	        case 'HR':
	            if( !empty($attr['WIDTH']) )
	                $Width = $attr['WIDTH'];
	            else
	                $Width = $this->w - $this->lMargin-$this->rMargin;
	            $x = $this->GetX();
	            $y = $this->GetY();
	            $this->SetLineWidth(0.2);
	            $this->Line($x,$y,$x+$Width,$y);
	            $this->SetLineWidth(0.2);
	            $this->Ln(1);
	            break;
	        case 'STRONG':
	            $this->SetStyle('B',true);
	            break;
	        case 'EM':
	            $this->SetStyle('I',true);
	            break;
	        case 'B':
	        case 'I':
	        case 'U':
	            $this->SetStyle($tag,true);
	            break;
	        case 'A':
	            $this->HREF=$attr['HREF'];
	            break;
	        case 'IMG':
	            if(isset($attr['SRC']) && (isset($attr['WIDTH']) || isset($attr['HEIGHT']))) {
	                if(!isset($attr['WIDTH']))
	                    $attr['WIDTH'] = 0;
	                if(!isset($attr['HEIGHT']))
	                    $attr['HEIGHT'] = 0;
	                $this->Image($attr['SRC'], $this->GetX(), $this->GetY(), px2mm($attr['WIDTH']), px2mm($attr['HEIGHT']));
	            }
	            break;
	        case 'BLOCKQUOTE':
	        case 'BR':
	            $this->Ln(5);
	            break;
	        case 'P':
	            $this->Ln(10);
	            break;
	        case 'FONT':
	            if (isset($attr['COLOR']) && $attr['COLOR']!='') {
	                $coul=hex2dec($attr['COLOR']);
	                $this->SetTextColor($coul['R'],$coul['G'],$coul['B']);
	                $this->issetcolor=true;
	            }
	            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist)) {
	                $this->SetFont(strtolower($attr['FACE']));
	                $this->issetfont=true;
	            }
	            if (isset($attr['FACE']) && in_array(strtolower($attr['FACE']), $this->fontlist) && isset($attr['SIZE']) && $attr['SIZE']!='') {
	                $this->SetFont(strtolower($attr['FACE']),'',$attr['SIZE']);
	                $this->issetfont=true;
	            }
	            break;
	    }
	}
	
	function CloseTag($tag)
	{
	    //Closing tag
	    if($tag=='SUP') {
	    }
	
	    if($tag=='TD') { // TD-END
	        $this->tdbegin=false;
	        $this->tdwidth=0;
	        $this->tdheight=0;
	        $this->tdalign="L";
	        $this->tdbgcolor=false;
	    }
	    if($tag=='TR') { // TR-END
	        $this->Ln();
	    }
	    if($tag=='TABLE') { // TABLE-END
	        //$this->Ln();
	        $this->tableborder=0;
	    }
	
	    if($tag=='STRONG')
	        $tag='B';
	    if($tag=='EM')
	        $tag='I';
	    if($tag=='B' || $tag=='I' || $tag=='U')
	        $this->SetStyle($tag,false);
	    if($tag=='A')
	        $this->HREF='';
	    if($tag=='FONT'){
	        if ($this->issetcolor==true) {
	            $this->SetTextColor(0);
	        }
	        if ($this->issetfont) {
	            $this->SetFont('arial');
	            $this->issetfont=false;
	        }
	    }
	}
	
	function SetStyle($tag, $enable)
	{
	    //Modify style and select corresponding font
	    $this->$tag+=($enable ? 1 : -1);
	    $style='';
	    foreach(array('B','I','U') as $s) {
	        if($this->$s>0)
	            $style.=$s;
	    }
	    $this->SetFont('',$style);
	}
	
	function PutLink($URL, $txt)
	{
	    //Put a hyperlink
	    $this->SetTextColor(0,0,255);
	    $this->SetStyle('U',true);
	    $this->Write(5,$txt,$URL);
	    $this->SetStyle('U',false);
	    $this->SetTextColor(0);
	}
	
	private function getHtmlTranslationTable() {
		$trans = get_html_translation_table(HTML_ENTITIES);
		$trans = array_flip ($trans);

		$trans['&sbquo;']  = ',';   // Single Low-9 Quotation Mark
		$trans['&fnof;']   = '';    // Latin Small Letter F With Hook
		$trans['&bdquo;']  = '';    // Double Low-9 Quotation Mark
		$trans['&hellip;'] = '...'; // Horizontal Ellipsis
		$trans['&dagger;'] = '';    // Dagger
		$trans['&Dagger;'] = '';    // Double Dagger
		$trans['&circ;']   = '^';   // Modifier Letter Circumflex Accent
		$trans['&permil;'] = '';    // Per Mille Sign
		$trans['&Scaron;'] = 'S';   // Latin Capital Letter S With Caron
		$trans['&lsaquo;'] = '<';   // Single Left-Pointing Angle Quotation Mark
		$trans['&OElig;']  = 'OE';  // Latin Capital Ligature OE
		$trans['&lsquo;']  = "'";   // Left Single Quotation Mark
		$trans['&rsquo;']  = "'";   // Right Single Quotation Mark
		$trans['&ldquo;']  = '"';   // Left Double Quotation Mark
		$trans['&rdquo;']  = '"';   // Right Double Quotation Mark
		$trans['&bull;']   = '-';   // Bullet
		$trans['&ndash;']  = '-';   // En Dash
		$trans['&mdash;']  = '-';   // Em Dash
		$trans['&tilde;']  = '~';   // Small Tilde
		$trans['&trade;']  = 'TM';  // Trade Mark Sign
		$trans['&scaron;'] = 's';   // Latin Small Letter S With Caron
		$trans['&rsaquo;'] = '>';   // Single Right-Pointing Angle Quotation Mark
		$trans['&oelig;']  = 'oe';  // Latin Small Ligature OE
		$trans['&Yuml;']   = 'Y';   // Latin Capital Letter Y With Diaeresis
		$trans['&nbsp;']   = ' ';   // non-breaking space
		$trans["&amp;"]    = "&";
		$trans["&lt;"]     = "<";
		$trans["&gt;"]     = ">";
		$trans["&laquo;"]  = "�";
		$trans["&raquo;"]  = "�";
		$trans["&para;"]   = "�";
		$trans["&euro;"]   = '�';
		$trans["&copy;"]   = "�";
		$trans["&reg;"]    = "�";
		$trans["&plusmn;"] = "�";
		$trans["&tilde;"]  = "~";
		$trans["&circ;"]   = "^";
		$trans["&quot;"]   = '"';
		$trans["&permil;"] = "?";
		$trans["�"]        = "'";
		$trans["�"]        = '-';


		// Reverse WordPress convert_chars() function
		$trans['&#8364;']  = $trans["&euro;"];
		$trans['&#8218;']  = $trans['&sbquo;'];
		$trans['&#402;']   = $trans['&fnof;'];
		$trans['&#8222;']  = $trans['&bdquo;'];
		$trans['&#8230;']  = $trans['&hellip;'];
		$trans['&#8224;']  = $trans['&dagger;'];
		$trans['&#8225;']  = $trans['&Dagger;'];
		$trans['&#710;']   = $trans['&circ;'];
		$trans['&#8240;']  = $trans['&permil;'];
		$trans['&#352;']   = $trans['&Scaron;'];
		$trans['&#8249;']  = $trans['&lsaquo;'];
		$trans['&#338;']   = $trans['&OElig;'];
		$trans['&#381;']   = 'Z'; // �
		$trans['&#8216;']  = $trans['&lsquo;'];
		$trans['&#8217;']  = $trans['&rsquo;'];
		$trans['&#8220;']  = $trans['&ldquo;'];
		$trans['&#8221;']  = $trans['&rdquo;'];
		$trans['&#8226;']  = $trans['&bull;'];
		$trans['&#8211;']  = $trans['&ndash;'];
		$trans['&#8212;']  = $trans['&mdash;'];
		$trans['&#732;']   = $trans['&tilde;'];
		$trans['&#8482;']  = $trans['&trade;'];
		$trans['&#353;']   = $trans['&scaron;'];
		$trans['&#8250;']  = $trans['&rsaquo;'];
		$trans['&#339;']   = $trans['&oelig;'];
		$trans['&#382;']   = 'z'; // �
		$trans['&#376;']   = $trans['&Yuml;'];

		return $trans;
	}
	
	
	private function unhtmlentities($string)  {
		$trans_tbl = $this->getHtmlTranslationTable();
		$ret = strtr ($string, $trans_tbl);
		return preg_replace('/&#(\d+);/me', "chr('\\1')",$ret);
	}
	
	
	private function convertCharset($string) {
		$string = preg_replace("|<br ?/?>|i", "\n", $string);
		$string = preg_replace("|^[\r\n]+|", "", $string);
		$string = preg_replace("|[\r\n]+|", "\n", $string);
		return iconv('UTF-8', self::PDF_ENCODING, $this->unhtmlentities($string));
	}
}//end of class