<?php
/**
 * MSC Attachment Request
 */
$body_class = '';

$file_id = (int) $_GET['file_id'];
$post_id = (int) $_GET['post_id'];

$lot = mred_get_lot($post_id);
if (!empty($lot)) {
	if (!empty($lot['type']) && !empty($lot['type']['slug'])) {
		$body_class = 'download-lot-type-' . $lot['type']['slug'];
	}
}
else {
	$page = get_post($post_id);
	if (!empty($page)) {
		$body_class = 'download-page-' . $page->post_name;
	}
}

?><!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8 no-js" <?php language_attributes(); ?>> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<!--[if lte IE 8]><meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=IE8" /><![endif]-->
<title><?php wp_title( '' ); // wp_title is filtered by includes/customizations.php risen_title() ?></title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); // prints out JavaScript, CSS, etc. as needed by WordPress, theme, plugins, etc. ?>
</head>

<body <?php body_class($body_class); ?>>
<?php


while ( have_posts() ) : the_post();
	the_content();
endwhile;

wp_footer();
?>

</body>
</html>
