��    E      D  a   l      �     �             	     
   (     3  	   7     A  	   N     X     `     m     q  	   z     �     �  
   �     �     �  	   �     �     �  
   �       	          Q     [   m  	   �     �     �     �  
                        '  	   3  &   =     d     p     t  !   y  '   �     �     �     �     �  !   	     .	     =	     Y	     f	     �	     �	     �	     �	     �	  #   �	     �	     �	  	   �	     �	     �	     

     
     
     7
  �  :
     3     H     b     t     �     �     �     �     �     �     �     �  	   �  
   �     �     �  
             1     O     ]     p     �     �     �     �  N   �  ]   	     g     s     �     �     �     �     �     �     �     �  4        6     B     F      K  %   l     �     �  )   �     �  .   �     %  &   ;     b  #   u     �  $   �     �     �     �  $   �  	   	       	        %     +     >     O  $   S     x     .   ,             <      *   :   '       7          $       5   B   4       &                            E      =            %   ?      #       2      9          0      8   -   +   A               	   3       )         @              /                     "         1   >           C                     
                           D   !   6   ;   (        Add a building Add a development Add a floor Add a lot Add a plan All Apartment Availability Available Balcony Balcony area Bld Building Buildings Development Developments Dimensions Download Info pack Download info pack Edit Page Edit building Edit development Edit floor Edit lot Edit plan Error Error: plugin "MEO Real Estate Developments" depends on the following" plugins.   Error: plugin "MEO Real Estate Developments" requires a newer version of PHP to be running. Filter by Find a building Find a development Find a floor Find a lot Find a plan Floor Floors Garden area Info pack Invalid email address or download code Invalid lot Lot Lots Minimal version of PHP required:  Missing or invalid plan details for lot No No building found No building found in the trash No development found No development found in the trash No floor found No floor found in the trash No lot found No lot found in the trash No plan found No plan found in the trash Pages: Plan Plans Please install and/or activate them Reserved Rooms Situation Sold Surface area Terrace area Yes Your server's PHP version:  p. Project-Id-Version: MEO real estate
POT-Creation-Date: 2014-07-21 18:24+0100
PO-Revision-Date: 2014-10-01 16:47+0100
Last-Translator: Peter Holberton <webdev@allmeo.com>
Language-Team: MEO <webdev@allmeo.com>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.8
X-Poedit-Basepath: .
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;mred_translate
X-Poedit-SearchPath-0: ..
 Ajouter un bâtiment Ajouter un développement Ajouter un étage Ajouter un lot Ajouter un plan Tous Appartement Disponibilité Libre Balcon Surface balcon Bât Bâtiment Bâtiments Développement Développements Dimensions Télécharger dossier complet Télécharger dossier complet Modifier page Modifier bâtiment Modifier développement Modifier étage Modifier lot Modifier plan Erreur Erreur: extension "MEO Real Estate Developments" dépend des plugins suivants. Erreur: extension "MEO Real Estate Developments" nécessite une version plus récente de PHP. Filtrer par Trouver un bâtiment Trouver un développement Trouver un étage Trouver un lot Trouver un plan Étage Étages Surface jardin Dossier complet Adresse mail ou un code de téléchargement invalide Lot invalid Lot Lots Version minimale de PHP requise: Plan manquant ou invalide pour le lot Non Aucun bâtiment trouvé Aucun bâtiment trouvée dans la poubelle Aucun développement trouvée Aucun développement trouvée dans la poubelle Aucun étage trouvée Aucun étage trouvée dans la poubelle Aucun lot trouvée Aucun lot trouvée dans la poubelle Aucun plan trouvée Aucun plan trouvée dans la poubelle Pages: Plan Plans Merci de les installer et/ou activer Réservé Pièces Situation Vendu Surface pondérée Surface terrasse Oui La version de PHP sur votre serveur: p. 