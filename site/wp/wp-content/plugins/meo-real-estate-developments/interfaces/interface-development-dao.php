<?php
interface DevelopmentDao {
	public function getDevelopments();
	public function getSectors();
	public function getBuildings();
	public function getFloors();
	public function getLots();
	public function getPlans();
	public function getEntries();

	public function getLotTypes();
}
