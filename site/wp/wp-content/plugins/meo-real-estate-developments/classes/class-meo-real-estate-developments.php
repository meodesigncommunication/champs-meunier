<?php
/*
Copyright (C) MEO design et communication Sàrl 2014. All rights reserved

Unauthorized copying of this file, via any medium is strictly prohibited
Proprietary and confidential

info@allmeo.com
*/


class MeoRealEstateDevelopments {

	private $dao;
	private $translator;
	private $smartcapture;


	public function __construct() {
		require_once( dirname(__FILE__) . '/class-development-dao-p2p.php');
		require_once( dirname(__FILE__) . '/class-wordpress-translator.php');
		require_once( dirname(__FILE__) . '/class-meo-smart-capture-api.php');
		$this->dao = new DevelopmentDaoP2P();
		$this->translator = new WordPressTranslator();
		$this->smartcapture = new MeoSmartCaptureApi($this);
	}

	public static function activate() {
		// Do nothing
	}

	public static function deactivate() {
		// Do nothing
	}
	

	public function reset() {
		$this->dao->reset();
		$this->dao = new DevelopmentDaoP2P();
		return true;
	}
	
	public function getDevelopments() {
		return $this->dao->getDevelopments();
	}

	public function getSectors() {
		return $this->dao->getSectors();
	}

	public function getBuildings() {
		return $this->dao->getBuildings();
	}

	public function getFloors() {
		return $this->dao->getFloors();
	}

	public function getLots() {
		return $this->dao->getLots();
	}

	public function getPlans() {
		return $this->dao->getPlans();
	}

	public function getEntries() {
		return $this->dao->getEntries();
	}

	public function getLotTypes() {
		return $this->dao->getLotTypes();
	}

	public function translate($phrase, $language = null) {
		return $this->translator->translate($phrase, $language);
	}

	public function getAjaxLotList() {
		$this->smartcapture->getAjaxLotList();
	}

	public function getAjaxContactList() {
		$this->smartcapture->getAjaxContactList();
	}

	public function getAjaxContact() {
		$this->smartcapture->getAjaxContact();
	}

	public function getAjaxDevelopment() {
		$this->smartcapture->getAjaxDevelopment();
	}

	public function getAjaxStatuses() {
		$this->smartcapture->getAjaxStatuses();
	}

	public function setAjaxStatus() {
		$this->smartcapture->setAjaxStatus();
	}

	public function setAjaxPrice() {
		$this->smartcapture->setAjaxPrice();
	}
}
