<?php
require_once(dirname(__FILE__) . '/../interfaces/interface-development-dao.php');

class DevelopmentDaoP2P implements DevelopmentDao {

	// Custom post types
	const CPT_DEVELOPMENT = 'development';
	const CPT_SECTOR      = 'sector';
	const CPT_BUILDING    = 'building';
	const CPT_FLOOR       = 'floor';
	const CPT_PLAN        = 'plan';
	const CPT_LOT         = 'lot';
	const CPT_ENTRY       = 'entry';

	// Custom taxonomies
	const TAX_LOT_TYPES   = 'lot_type';

	// Relationships
	const REL_SECTOR_DEV      = 'sectors_to_developments';
	const REL_BUILDING_SECTOR = 'buildings_to_sectors';
	const REL_FLOOR_BUILDING  = 'floors_to_buildings';
	const REL_LOT_FLOOR       = 'lots_to_floors';
	const REL_LOT_PLAN        = 'lots_to_plans';
	const REL_ENTRY_BUILDING  = 'entries_to_buildings';
	const REL_LOT_ENTRY       = 'lots_to_entries';

	private $developments = array();
	private $sectors = array();
	private $buildings = array();
	private $floors = array();
	private $lots = array();
	private $plans = array();
	private $entries = array();
	private $lot_types = array();

	private $qtranslate_active;
	private $acf_qtranslate_active;

	public function __construct() {
		if (!function_exists('is_plugin_active')) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		}
		$this->qtranslate_active = ( is_plugin_active('qtranslate/qtranslate.php') || is_plugin_active('mqtranslate/mqtranslate.php') );
		$this->acf_qtranslate_active = is_plugin_active('acf-qtranslate/acf-qtranslate.php');

		add_action('init', array(&$this, 'createCustomPostTypes'), 5);
		add_action('p2p_init', array(&$this, 'createRelationships'));
	}

	public function reset(){
		global $post;

		$this->developments = array();
		$this->sectors = array();
		$this->buildings = array();
		$this->floors = array();
		$this->lots = array();
		$this->plans = array();
		$this->entries = array();
		$this->lot_types = array();

		return true;
	}

	public function getDevelopments() {
		global $post;

		if (!empty($this->developments)) {
			return $this->developments;
		}

		$this->developments = array();

		$args = array(
			'post_type' => self::CPT_DEVELOPMENT,
			'posts_per_page' => -1,
			'meta_key' => 'code',
			'orderby' => 'meta_value',
			'order' => 'ASC'
		);

		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$development = array();
			$development['id'] = $post->ID;
			$development['name'] = get_the_title();
			$development['slug'] = $post->post_name;
			$development['url'] = get_permalink();

			foreach(array('code', 'plan', 'plan_hover_transparent') as $field) {
				$development[$field] = get_field($field, $post->ID);
			}

			$this->developments[$post->ID] = $development;
		}
		wp_reset_postdata();

		return $this->developments;
	}

	public function getSectors() {
		global $post;

		if (!empty($this->sectors)) {
			return $this->sectors;
		}

		$this->sectors = array();

		$args = array(
			'post_type' => self::CPT_SECTOR,
			'posts_per_page' => -1,
			'meta_key' => 'code',
			'orderby' => 'meta_value',
			'order' => 'ASC'
		);

		$loop = new WP_Query( $args );
		p2p_type( self::REL_SECTOR_DEV )->each_connected( $loop );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$sector = array();
			$sector['id'] = $post->ID;
			$sector['name'] = get_the_title();
			$sector['slug'] = $post->post_name;
			$sector['url'] = get_permalink();

			foreach(array('code') as $field) {
				$sector[$field] = get_field($field, $post->ID);
			}

			if (!empty($post->connected)) {
				$sector['development_id'] = $post->connected[0]->ID;
			}

			$this->sectors[$post->ID] = $sector;
		}
		wp_reset_postdata();

		return $this->sectors;
	}

	public function getBuildings() {
		global $post;

		if (!empty($this->buildings)) {
			return $this->buildings;
		}

		$this->buildings = array();

		$args = array(
			'post_type' => self::CPT_BUILDING,
			'posts_per_page' => -1,
			'meta_key' => 'code',
			'orderby' => 'meta_value',
			'order' => 'ASC'
		);

		$loop = new WP_Query( $args );
		p2p_type( self::REL_BUILDING_SECTOR )->each_connected( $loop );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$building = array();
			$building['id'] = $post->ID;
			$building['name'] = get_the_title();
			$building['slug'] = $post->post_name;
			$building['url'] = get_permalink();

			foreach(array('code', 'building_image', 'top_floor_offset', 'css_floor_height', 'dev_plan_coordinates', 'dev_plan_hover_image') as $field) {
				$building[$field] = get_field($field, $post->ID);
			}

			if (!empty($post->connected)) {
				$building['sector_id'] = $post->connected[0]->ID;
			}

			$lot_types = array();
			$lot_type_objects = get_the_terms($post->ID, self::TAX_LOT_TYPES);
			if (!empty($lot_type_objects)) {
				foreach($lot_type_objects as $lot_type_object) {
					$lot_types[$lot_type_object->term_id] = array(
						'id'   => $lot_type_object->term_id,
						'name' => $lot_type_object->name,
						'slug' => $lot_type_object->slug
					);
				}
			}

			$building['lot_types'] = $lot_types;

			$this->buildings[$post->ID] = $building;
		}
		wp_reset_postdata();

		return $this->buildings;
	}

	public function getFloors() {
		global $post;

		if (!empty($this->floors)) {
			return $this->floors;
		}

		$this->floors = array();

		$args = array(
			'post_type' => self::CPT_FLOOR,
			'posts_per_page' => -1,
			'meta_key' => 'order',
			'orderby' => 'meta_value',
			'order' => 'ASC'
		);

		$loop = new WP_Query( $args );
		p2p_type( self::REL_FLOOR_BUILDING )->each_connected( $loop );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$floor = array();
			$floor['id'] = $post->ID;
			$floor['name'] = get_the_title();
			$floor['slug'] = $post->post_name;

			foreach(array('code', 'order', 'plan', 'ordinal', 'dev_plan_coordinates', 'dev_plan_hover_image', 'walls_image', 'floor_overlay_image') as $field) {
				$floor[$field] = get_field($field, $post->ID);
			}
			if (!empty($post->connected)) {
				$floor['building_id'] = $post->connected[0]->ID;
			}
			$this->floors[$post->ID] = $floor;
		}
		wp_reset_postdata();

		return $this->floors;
	}


	public function getLots() {
		global $post;

		if (!empty($this->lots)) {
			return $this->lots;
		}

		$this->lots = array();

		$args = array(
			'post_type' => self::CPT_LOT,
			'posts_per_page' => -1
		);

		add_filter('posts_clauses', array($this, 'orderByLotType'), 10, 2 );

		$loop = new WP_Query( $args );

		remove_filter('posts_clauses', array($this, 'orderByLotType'), 10, 2 );


		if (!p2p_type( self::REL_LOT_PLAN )) {
			// CF7 does something funny with the load order when making ajax requests,
			// and p2p_init doesn't fire.  In that case, just create the relationships
			$this->createRelationships();
		}

		p2p_type( self::REL_LOT_PLAN )->each_connected( $loop, array(), 'plans' );
		p2p_type( self::REL_LOT_FLOOR )->each_connected( $loop, array(), 'floors' );
		p2p_type( self::REL_LOT_ENTRY )->each_connected( $loop, array(), 'entries' );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$lot = array();
			$lot['id'] = $post->ID;
			$lot['name'] = get_the_title();
			$lot['url'] = get_permalink();
			$lot['slug'] = $post->post_name;

			foreach(array('code', 'opus_code', 'pieces', 'surface_interior', 'surface_balcony', 'surface_terrace', 'surface_garden', 'surface_weighted', 'svg_id', 'pdf', 'floor_position_image', 'floor_position_image_fallback', 'lot_situation_image', 'lot_situation_image_fallback') as $field) {
				$lot[$field] = get_field($field, $post->ID);
			}
			// Fields that can be set via ajax using update_post_meta.  ACF's get_field() may not return the latest values
			// Don't use get_post_meta() for all fields, as get_field() can return objects (eg for images), not just get_post_meta() values
			// TODO: look at changing the set calls to ACF's update_field()
			foreach(array('availability', 'price') as $field) {
				$lot[$field] = get_post_meta( $post->ID, $field, true );
			}


			if (!empty($post->plans)) {
				$lot['plan_id'] = $post->plans[0]->ID;
			}
			if (!empty($post->floors)) {
				$lot['floor_id'] = $post->floors[0]->ID;
			}
			if (!empty($post->entries)) {
				$lot['entry_id'] = $post->entries[0]->ID;
			}

			$lot_type = array();
			$lot_types = get_the_terms($post->ID, self::TAX_LOT_TYPES);
			if (!empty($lot_types)) {
				$first_lot_type = reset($lot_types); // Assume there's only one

				if (!empty($first_lot_type)) {
					$lot_type['id'] = $first_lot_type->term_id;
					$lot_type['name'] = $first_lot_type->name;
					$lot_type['slug'] = $first_lot_type->slug;
				}
			}

			$lot['type'] = $lot_type;

			$this->lots[$post->ID] = $lot;
		}
		wp_reset_postdata();

		return $this->lots;

	}

	public function orderByLotType( $clauses, $wp_query ) {
		global $wpdb;

            $clauses['join'] .=<<<SQL
LEFT OUTER JOIN {$wpdb->term_relationships} tr ON {$wpdb->posts}.ID = tr.object_id
LEFT OUTER JOIN {$wpdb->term_taxonomy} tt USING (term_taxonomy_id)
LEFT OUTER JOIN {$wpdb->terms} t USING (term_id)
SQL;
            $clauses['where'] .= " AND (taxonomy = 'lot_type' OR taxonomy IS NULL)";
            $clauses['groupby'] = "{$wpdb->posts}.ID";
            $clauses['orderby'] = "t.term_id ASC";

		return $clauses;
	}

	public function getPlans() {
		global $post;

		if (!empty($this->plans)) {
			return $this->plans;
		}

		$this->plans = array();

		$args = array(
			'post_type' => self::CPT_PLAN,
			'posts_per_page' => -1,
			'meta_key' => 'code',
			'orderby' => 'meta_value',
			'order' => 'ASC'
		);

		$loop = new WP_Query( $args );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$plan = array();
			$plan['id'] = $post->ID;
			$plan['name'] = get_the_title();
			$plan['slug'] = $post->post_name;

			foreach(array('code', 'detailed_plan_image', 'detailed_plan_image_fallback', 'pdf', 'svg_id', 'image_3d') as $field) {
				$plan[$field] = get_field($field, $post->ID);
			}

			$this->plans[$post->ID] = $plan;
		}
		wp_reset_postdata();

		return $this->plans;
	}

	public function getEntries() {
		global $post;

		if (!empty($this->entries)) {
			return $this->entries;
		}

		$this->entries = array();

		$args = array(
			'post_type' => self::CPT_ENTRY,
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC'
		);

		$loop = new WP_Query( $args );
		p2p_type( self::REL_ENTRY_BUILDING )->each_connected( $loop, array(), 'buildings' );
		while ( $loop->have_posts() ) {
			$loop->the_post();

			$entry = array();
			$entry['id'] = $post->ID;
			$entry['name'] = get_the_title();
			$entry['slug'] = $post->post_name;

			foreach(array('code') as $field) {
				$entry[$field] = get_field($field, $post->ID);
			}

			if (!empty($post->buildings)) {
				$entry['building_id'] = $post->buildings[0]->ID;
			}

			$this->entries[$post->ID] = $entry;
		}
		wp_reset_postdata();

		return $this->entries;
	}

	public function getLotTypes() {
		if (!empty($this->lot_types)) {
			return $this->lot_types;
		}

		$raw_lot_types =  get_terms( self::TAX_LOT_TYPES, array(
			'hide_empty' => 0
		));

		// Could just return objects, but I'd rather be consistent
		// with other elements - so array
		$this->lot_types = array();
		foreach ($raw_lot_types as $raw_lot_type) {
			$lot_type = array(
				'id'   => $raw_lot_type->term_id,
				'name' => $raw_lot_type->name,
				'slug' => $raw_lot_type->slug
			);

			$this->lot_types[$raw_lot_type->term_id] = $lot_type;
		}

		return $this->lot_types;
	}


	/* ---------------------------------------------------------------------------------------------
	 * Setup functions
	 * --------------------------------------------------------------------------------------------- */
	public function createCustomPostTypes() {
		$this->createDevelopmentCustomPostType();
		$this->createSectorCustomPostType();
		$this->createBuildingCustomPostType();
		$this->createFloorCustomPostType();
		$this->createPlanCustomPostType();
		$this->createLotCustomPostType();
		$this->createEntryCustomPostType();

		$this->createLotTypeTaxonomy();
	}

	private function getMultilingualFieldType($type) {
		$equivalents = array(
			'text'     => 'qtranslate_text',
			'textarea' => 'qtranslate_textarea',
			'wysiwyg'  => 'qtranslate_wysiwyg',
			'image'    => 'qtranslate_image',
		);
		if (!$this->acf_qtranslate_active or !isset($equivalents[$type])) {
			return $type;
		}
		return $equivalents[$type];
	}

	private function createDevelopmentCustomPostType() {
		register_post_type(self::CPT_DEVELOPMENT, array(
			'labels' => array(
				'name' => __('Developments', MRED_TEXT_DOMAIN),
				'singular_name' => __('Development', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a development', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit development', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a development', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a development', MRED_TEXT_DOMAIN),
				'not_found' => __('No development found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No development found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => true,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title', 'editor')
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_development-details',
				'title' => 'Development details',
				'fields' => $this->getDevelopmentFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_DEVELOPMENT,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getDevelopmentFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537ccb680800d',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53fb19043142b',
						'label' => 'plan',
						'name' => 'plan',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5418ab8a2c2cc',
						'label' => 'Transparent PNG of plan size',
						'name' => 'plan_hover_transparent',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				);
	}

	private function createSectorCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_SECTOR, array(
			'labels' => array(
				'name' => __('Sectors', MRED_TEXT_DOMAIN),
				'singular_name' => __('Sector', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add an sector', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit sector', MRED_TEXT_DOMAIN),
				'new_item' => __('Add an sector', MRED_TEXT_DOMAIN),
				'search_items' => __('Find an sector', MRED_TEXT_DOMAIN),
				'not_found' => __('No sector found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No sector found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_sector-details',
				'title' => 'Sector details',
				'fields' => $this->getSectorFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_SECTOR,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getSectorFieldDefinitions() {
		return  array (
			array (
				'key' => 'field_54dc9adc38ea6',
				'label' => 'Code',
				'name' => 'code',
				'prefix' => '',
				'type' => 'text',
				'instructions' => '',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array (
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
				'readonly' => 0,
				'disabled' => 0,
			)
		);
	}

	private function createBuildingCustomPostType() {
		register_post_type(self::CPT_BUILDING, array(
			'labels' => array(
				'name' => __('Buildings', MRED_TEXT_DOMAIN),
				'singular_name' => __('Building', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a building', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit building', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a building', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a building', MRED_TEXT_DOMAIN),
				'not_found' => __('No building found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No building found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => true,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => array('title', 'editor')
		));


		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_building-details',
				'title' => 'Building details',
				'fields' => $this->getBuildingFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_BUILDING,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getBuildingFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537cc5f6f641d',
						'label' => 'Code',
						'name' => 'code',
						'type' => $this->getMultilingualFieldType('text'),
						'required' => 1,
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53bd10a7c489b',
						'label' => 'Building image',
						'name' => 'building_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_53bd2ef64700c',
						'label' => 'CSS top floor offset (%)',
						'name' => 'top_floor_offset',
						'type' => 'text',
						'instructions' => 'Numeric (can be decimal)',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53bd2f154700d',
						'label' => 'CSS Floor height (%)',
						'name' => 'css_floor_height',
						'type' => 'text',
						'instructions' => 'Numeric (can be decimal)',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53fb2826fc04d',
						'label' => 'Development plan coordinates',
						'name' => 'dev_plan_coordinates',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5418a78298667',
						'label' => 'Development plan hover image',
						'name' => 'dev_plan_hover_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				);
	}

	private function createFloorCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_FLOOR, array(
			'labels' => array(
				'name' => __('Floors', MRED_TEXT_DOMAIN),
				'singular_name' => __('Floor', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a floor', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit floor', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a floor', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a floor', MRED_TEXT_DOMAIN),
				'not_found' => __('No floor found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No floor found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_floor-details',
				'title' => 'Floor details',
				'fields' => $this->getFloorFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_FLOOR,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}

	}

	public function getFloorFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537cd163db6f9',
						'label' => 'Code',
						'name' => 'code',
						'type' => $this->getMultilingualFieldType('text'),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537cd16ddb6fa',
						'label' => 'Ordinal',
						'name' => 'ordinal',
						'type' => $this->getMultilingualFieldType('text'),
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537cd17bdb6fb',
						'label' => 'Order',
						'name' => 'order',
						'type' => 'number',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'min' => '',
						'max' => '',
						'step' => '',
					),
					array (
						'key' => 'field_5395c668b0fe1',
						'label' => 'Plan',
						'name' => 'plan',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5409d54c4ee71',
						'label' => 'Development plan coordinates',
						'name' => 'dev_plan_coordinates',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_5418a82228fa6',
						'label' => 'Development plan hover image',
						'name' => 'dev_plan_hover_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54eb24bc34219',
						'label' => 'Walls Image',
						'name' => 'walls_image',
						'prefix' => '',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
					),
					array (
						'key' => 'field_54eb25b3cddf4',
						'label' => 'Floor overlay image',
						'name' => 'floor_overlay_image',
						'prefix' => '',
						'type' => 'image',
						'instructions' => 'Transparent PNG, the same size as the floor PNG (walls image)',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'medium',
						'library' => 'all',
					),
				);
	}

	private function createPlanCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_PLAN, array(
			'labels' => array(
				'name' => __('Plans', MRED_TEXT_DOMAIN),
				'singular_name' => __('Plan', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a plan', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit plan', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a plan', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a plan', MRED_TEXT_DOMAIN),
				'not_found' => __('No plan found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No plan found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_plan-details',
				'title' => 'Plan details',
				'fields' => $this->getPlanFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_PLAN,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}

	}

	public function getPlanFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537db184db1ce',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537db189db1cf',
						'label' => 'Detailed plan image (SVG)',
						'name' => 'detailed_plan_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54e75a810ec2f',
						'label' => 'Detailed plan image fallback',
						'name' => 'detailed_plan_image_fallback',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_537db1b8db1d0',
						'label' => 'PDF',
						'name' => 'pdf',
						'type' => 'file',
						'save_format' => 'object',
						'library' => 'all',
					),
					array (
						'key' => 'field_53bbf7a6da5dc',
						'label' => 'SVG ID',
						'name' => 'svg_id',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55896a273ba7d',
						'label' => '3D image',
						'name' => 'image_3d',
						'prefix' => '',
						'type' => 'image',
						'instructions' => '',
						'required' => 0,
						'conditional_logic' => 0,
						'wrapper' => array (
							'width' => '',
							'class' => '',
							'id' => '',
						),
						'return_format' => 'array',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				);
	}

	private function createLotCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_LOT, array(
			'labels' => array(
				'name' => __('Lots', MRED_TEXT_DOMAIN),
				'singular_name' => __('Lot', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add a lot', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit lot', MRED_TEXT_DOMAIN),
				'new_item' => __('Add a lot', MRED_TEXT_DOMAIN),
				'search_items' => __('Find a lot', MRED_TEXT_DOMAIN),
				'not_found' => __('No lot found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No lot found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => true,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_lot-details',
				'title' => 'Lot details',
				'fields' => $this->getLotFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_LOT,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getLotFieldDefinitions() {
		return array (
					array (
						'key' => 'field_537da1f32738f',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da1fd27390',
						'label' => 'Opus code',
						'name' => 'opus_code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da20727391',
						'label' => 'Rooms',
						'name' => 'pieces',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da21727392',
						'label' => 'Surface interior',
						'name' => 'surface_interior',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da22227393',
						'label' => 'Surface balcony',
						'name' => 'surface_balcony',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da22d27394',
						'label' => 'Surface terrace',
						'name' => 'surface_terrace',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da23727395',
						'label' => 'Surface garden',
						'name' => 'surface_garden',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da24827396',
						'label' => 'Surface weighted',
						'name' => 'surface_weighted',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_537da25227397',
						'label' => 'Availability',
						'name' => 'availability',
						'type' => 'radio',
						'choices' => array (
							'available' => 'Available',
							'reserved' => 'Reserved',
							'sold' => 'Sold',
						),
						'other_choice' => 0,
						'save_other_choice' => 0,
						'default_value' => '',
						'layout' => 'vertical',
					),
					array (
						'key' => 'field_53bce5f575b49',
						'label' => 'SVG ID',
						'name' => 'svg_id',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_53df4a7b6ab08',
						'label' => 'PDF',
						'name' => 'pdf',
						'type' => 'file',
						'save_format' => 'object',
						'library' => 'all',
					),
					array (
						'key' => 'field_53ef3ace5f393',
						'label' => 'Floor position image',
						'name' => 'floor_position_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_53ef433ab518b',
						'label' => 'Floor position image fallback',
						'name' => 'floor_position_image_fallback',
						'type' => 'image',
						'instructions' => 'For browsers that do not support SVG',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54f55e45ce2f4',
						'label' => 'Lot situation image',
						'name' => 'lot_situation_image',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_54f55e81ce2f5',
						'label' => 'Lot situation image fallback',
						'name' => 'lot_situation_image_fallback',
						'type' => 'image',
						'instructions' => 'For browsers that do not support SVG',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_5524d33209c10',
						'label' => 'Price',
						'name' => 'price',
						'type' => 'text',
						'default_value' => '',
						'instructions' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					)
				);
	}

	private function createEntryCustomPostType() {
		$supports = array('title');

		// qTranslate won't fire up for other fields unless the editor is present
		if ($this->qtranslate_active) {
			$supports[] = 'editor';
		}

		register_post_type(self::CPT_ENTRY, array(
			'labels' => array(
				'name' => __('Entries', MRED_TEXT_DOMAIN),
				'singular_name' => __('Entry', MRED_TEXT_DOMAIN),
				'add_new_item' => __('Add an entry', MRED_TEXT_DOMAIN),
				'edit_item' => __('Edit entry', MRED_TEXT_DOMAIN),
				'new_item' => __('Add an entry', MRED_TEXT_DOMAIN),
				'search_items' => __('Find an entry', MRED_TEXT_DOMAIN),
				'not_found' => __('No entry found', MRED_TEXT_DOMAIN),
				'not_found_in_trash' => __('No entry found in the trash', MRED_TEXT_DOMAIN),
			),
			'public' => false,
			'hierarchical' => false,
			'exclude_from_search' => true,
			'query_var' => true,
			'show_ui' => true,
			'can_export' => true,
			'supports' => $supports
		));

		if(function_exists("register_field_group"))
		{
			register_field_group(array (
				'id' => 'acf_entry-details',
				'title' => 'Entry details',
				'fields' => $this->getEntryFieldDefinitions(),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => self::CPT_ENTRY,
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));
		}
	}

	public function getEntryFieldDefinitions() {
		return array (
					array (
						'key' => 'field_53d8f759aa361',
						'label' => 'Code',
						'name' => 'code',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				);
	}

	private function createLotTypeTaxonomy() {
		$applies_to = array(
			self::CPT_BUILDING,
			self::CPT_LOT
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => array(
				'name'              => __( 'Lot types', MRED_TEXT_DOMAIN ),
				'singular_name'     => __( 'Lot type', MRED_TEXT_DOMAIN ),
				'search_items'      => __( 'Search lot types', MRED_TEXT_DOMAIN ),
				'all_items'         => __( 'All lot types', MRED_TEXT_DOMAIN ),
				'parent_item'       => __( 'Parent lot type', MRED_TEXT_DOMAIN ),
				'parent_item_colon' => __( 'Parent lot type:', MRED_TEXT_DOMAIN ),
				'edit_item'         => __( 'Edit lot type', MRED_TEXT_DOMAIN ),
				'update_item'       => __( 'Update lot type', MRED_TEXT_DOMAIN ),
				'add_new_item'      => __( 'Add New lot type', MRED_TEXT_DOMAIN ),
				'new_item_name'     => __( 'New lot type Name', MRED_TEXT_DOMAIN ),
				'menu_name'         => __( 'Lot types', MRED_TEXT_DOMAIN ),
			),
			'show_ui'           => true,
			'show_admin_column' => true,
		);


		register_taxonomy( self::TAX_LOT_TYPES, $applies_to, $args );
	}


	public function createRelationships() {
		if (! function_exists('p2p_register_connection_type')) {
			error_log("p2p_register_connection_type function doesn't exist");
			return;
		}

		$relationships = array(
			self::REL_SECTOR_DEV => array(
				'from' => self::CPT_SECTOR,
				'to' => self::CPT_DEVELOPMENT
			),
			self::REL_BUILDING_SECTOR => array(
				'from' => self::CPT_BUILDING,
				'to' => self::CPT_SECTOR
			),
			self::REL_FLOOR_BUILDING => array(
				'from' => self::CPT_FLOOR,
				'to' => self::CPT_BUILDING
			),
			self::REL_LOT_FLOOR => array(
				'from' => self::CPT_LOT,
				'to' => self::CPT_FLOOR
			),
			self::REL_LOT_PLAN => array(
				'from' => self::CPT_LOT,
				'to' => self::CPT_PLAN
			),
			self::REL_LOT_ENTRY => array(
				'from' => self::CPT_LOT,
				'to' => self::CPT_ENTRY
			),
			self::REL_ENTRY_BUILDING => array(
				'from' => self::CPT_ENTRY,
				'to' => self::CPT_BUILDING
			)
		);

		foreach ($relationships as $relationship_name => $relationship_details) {
			p2p_register_connection_type( array(
				'name' => $relationship_name,
				'from' => $relationship_details['from'],
				'to' => $relationship_details['to'],
				'admin_column' => 'from',
				'cardinality' => 'many-to-one'
			) );
		}
	}
}
