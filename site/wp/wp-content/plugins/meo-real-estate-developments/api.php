<?php
if (!class_exists('MeoRealEstateException')) {
	class MeoRealEstateException extends Exception {
		// pass
	}
}

function mred_reset() {
	global $mred;
	if (empty($mred) || !is_a($mred, 'MeoRealEstateDevelopments')) {
		throw new MeoRealEstateException("Plugin hasn't been initialised correctly (\$mred incorrect)");
	}
	return $mred->reset();
}

function mred_get_cpt($function, $exclusions) {
	global $mred;
	if (empty($mred) || !is_a($mred, 'MeoRealEstateDevelopments')) {
		throw new MeoRealEstateException("Plugin hasn't been initialised correctly (\$mred incorrect)");
	}
	$raw = $mred->{$function}();

	$result = array();
	foreach ($raw as $id => $item) {
		foreach ($exclusions as $exclusion) {
			unset($item[$exclusion]);
		}
		$result[$id] = $item;
	}

	return $result;
}

function mred_get_developments($exclusions = array()) {
	return mred_get_cpt('getDevelopments', $exclusions);
}

function mred_get_development($development_id, $exclusions = array()) {
	$developments = mred_get_developments($exclusions);
	return $developments[$development_id];
}


function mred_get_sectors($exclusions = array()) {
	return mred_get_cpt('getSectors', $exclusions);
}

function mred_get_sector($sector_id, $exclusions = array()) {
	$sectors = mred_get_sectors($exclusions);
	return $sectors[$sector_id];
}


function mred_get_buildings($exclusions = array()) {
	return mred_get_cpt('getBuildings', $exclusions);
}

function mred_get_building($building_id, $exclusions = array()) {
	$buildings = mred_get_buildings($exclusions);
	return $buildings[$building_id];
}


function mred_get_floors($exclusions = array()) {
	return mred_get_cpt('getFloors', $exclusions);
}

function mred_get_floor($floor_id, $exclusions = array()) {
	$floors = mred_get_floors($exclusions);
	return $floors[$floor_id];
}

function mred_get_floors_for_building($building_id, $exclusions = array()) {
	$floors = mred_get_floors($exclusions);
	$result = array();
	foreach ($floors as $floor) {
		if ($floor['building_id'] == $building_id) {
			$result[] = $floor;
		}
	}
	return $result;
}


function mred_get_lots($exclusions = array()) {
	return mred_get_cpt('getLots', $exclusions);
}

function mred_get_lot($lot_id, $exclusions = array()) {
	$lots = mred_get_lots($exclusions);
	return $lots[$lot_id];
}


function mred_get_plans($exclusions = array()) {
	return mred_get_cpt('getPlans', $exclusions);
}

function mred_get_plan($plan_id, $exclusions = array()) {
	$plans = mred_get_plans($exclusions);
	return $plans[$plan_id];
}


function mred_get_entries($exclusions = array()) {
	return mred_get_cpt('getEntries', $exclusions);
}

function mred_get_entry($entry_id, $exclusions = array()) {
	$entries = mred_get_entries($exclusions);
	return $entries[$entry_id];
}

function mred_get_lot_types() {
	return mred_get_cpt('getLotTypes', array());
}


function mred_translate($phrase, $language = null) {
	global $mred;
	if (empty($mred) || !is_a($mred, 'MeoRealEstateDevelopments')) {
		throw new MeoRealEstateException("Plugin hasn't been initialised correctly (\$mred incorrect)");
	}
	return $mred->translate($phrase, $language);
}

function mred_get_availability_descriptions() {
	$statuses = array (
		'available' => mred_translate('Available'),
		'reserved'  => mred_translate('Reserved'),
		'sold'      => mred_translate('Sold')
	);

	return $statuses;
}

function mred_get_availability_class($status) {
	$statuses = mred_get_availability_descriptions();

	if (!array_key_exists($status, $statuses)) {
		$status = 'sold'; // err on the side of caution
	}
	return $status;
}

function mred_get_availability_description($status) {
	$statuses = mred_get_availability_descriptions();

	$valid_status = mred_get_availability_class($status);

	return $statuses[$valid_status];
}

function mred_get_page_id_from_slug($slug, $post_type = 'page') {
	global $slug_to_id, $wpdb;
	if (!isset($slug_to_id[$slug])) {
		$slug_to_id[$slug] = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s", $slug, $post_type ) );
	}
	return $slug_to_id[$slug];
}

function mred_get_plugin_images_url() {
	return plugins_url('images', __FILE__);
}

function mred_get_url_from_filename($filename) {
	// We know we've uploaded (eg) filename.png, but don't know which uploads subdirectory it's in
	global $wpdb;

	$filename = '%/' . $filename;

	$sql = "
	    SELECT p.ID
	      FROM $wpdb->posts p
	           join $wpdb->postmeta pm on p.ID = pm.post_id
	     WHERE pm.meta_key = '_wp_attached_file'
	       AND pm.meta_value like %s
	       AND p.post_type = 'attachment'";

	$attachment_id = $wpdb->get_var( $wpdb->prepare( $sql, $filename ) );

	$url = wp_get_attachment_url( $attachment_id );

	return $url;
}

function mred_extract_options( $defaults, $user_provided ) {
	$user_provided = (array)$user_provided;
	$result = array();
	foreach($defaults as $name => $default) {
		if ( array_key_exists($name, $user_provided) ) {
			$result[$name] = $user_provided[$name];
		}
		else {
			$result[$name] = $default;
		}
	}

	return $result;
}

function mred_get_piwik_code($is_production) {
	global $wp_query;

	$piwik_server = "piwik.meoanalytics.com"; // TODO: add dev server
	$piwik_site_id = get_field('piwik_site_id', 'option');
	if (empty($piwik_site_id)) {
		return;
	}
	?>
	<!-- Piwik -->
	<script type="text/javascript">
		var analytics_post_id = <?php echo $wp_query->post->ID; ?>,
		    analytics_post_type = '<?php echo $wp_query->post->post_type; ?>';

		var triesCounter = 0;
		piwikload = function() {
			if (typeof jQuery === "undefined") {
				triesCounter++;
				if (triesCounter < 20) {
					setTimeout(piwikload, 50);
				}
			}
			else {
				var $forms = jQuery('form.wpcf7-form');
				if ($forms.length !== 0) {
					$forms.each(function () {
						jQuery('<input>').attr({
							type: 'hidden',
							name: 'analytics_id',
							value: "" + piwik_user_id
						}).appendTo(jQuery(this));
					});
				}
			}
		};
		var _paq = _paq || [];
		_paq.push(['setCustomVariable', '1', 'post_id',   analytics_post_id,   "page"]);
		_paq.push(['setCustomVariable', '2', 'post_type', analytics_post_type, "page"]);
		_paq.push(['trackPageView']);
		_paq.push(['enableLinkTracking']);
		_paq.push([ function() {
			piwik_user_id = this.getVisitorId();
			piwikload();
		}]);
		(function() {
			var u=(("https:" == document.location.protocol) ? "https" : "http") + "://<?php echo $piwik_server; ?>/";
			_paq.push(['setTrackerUrl', u+'piwik.php']);
			_paq.push(['setSiteId', <?php echo $piwik_site_id; ?>]);
			var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
			g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
		})();
	</script>
	<noscript><p><img src="http://piwik.meoanalytics.com/piwik.php?idsite=<?php echo $piwik_site_id; ?>" style="border:0;" alt="" /></p></noscript>
	<!-- End Piwik Code --><?php
}
