<?php
/**
 * Template Name: Plan list
 */

function mredtemplate_get_lot_list($tabs) {

	$upload_directory = wp_upload_dir();
	$cache_dir = $upload_directory['basedir'] . '/cache/html';
	
	// Update : Two cache files now, with and without prices
	if(isset($_SESSION['pricesEnabled']) && $_SESSION['pricesEnabled'] === true){
		$cache_file = $cache_dir . '/lot-list-price.html';
	}
	else $cache_file = $cache_dir . '/lot-list.html';
	
	wp_mkdir_p($cache_dir);
	if (file_exists($cache_file)) {
		echo file_get_contents($cache_file);
		return;
	}

	ob_start();

	$buildings = mred_get_buildings();

	$building_url = '';
	foreach ($buildings as $building) {
		foreach ($building['lot_types'] as $lot_type) {
			if ($lot_type['slug'] == 'appartement') {
				$building_url = $building['url'];
			}
		}
	}

	if (empty($building_url)) {
		$first_building = reset($buildings);
		$building_url = $first_building['url'];
	}
	?>
	<div class="filters-wrapper">
		<div class="filters">
			<div class="filter-responsive-link"><?php _e('Filter by', MREDTEMPLATES_TEXT_DOMAIN); ?></div>
			<span class="filter-label"><?php _e('Filter by', MREDTEMPLATES_TEXT_DOMAIN); ?></span>
			<?php echo mred_get_plan_list_filters(); ?>
		</div>
	</div>

	<?php

	while ( have_posts() ) : the_post(); ?>

	<div id="theme-page">
		<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper full-layout mk-grid vc_row-fluid">
			<div class="theme-content" itemprop="mainContentOfPage">

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

					<div class="wpb_row  vc_row-fluid  mk-fullwidth-false add-padding-0 attched-false">
						<div class="vc_span12 wpb_column column_container ">
							<h1 class="page-title"><span class="page-name"></span><span class="active-filter"><?php _e('Recherche par:', 'champs-meunier'); ?> <span class="filter-value">&nbsp;</span></span></h1>
							<div class="view-switchers">
								<span class="label"><?php _e('Display by', MREDTEMPLATES_TEXT_DOMAIN); ?></span>
								<a href="<?php echo $building_url; ?>" class="view-switcher building">building</a>
								<?php
								foreach ($tabs as $tab) { ?>
									<a href="#<?php echo $tab['tab-id']; ?>" class="view-switcher in-page <?php echo $tab['class']; echo $tab['active'] ? ' active' : ''; ?>" data-analytics-id="<?php echo $tab['analytics-id']; ?>"><?php echo $tab['label']; ?></a><?php
								} ?>
							</div>
							<div class="page-content"><?php the_content(); ?></div>
						</div>
					</div>

					<div class="views"><?php
						foreach ($tabs as $tab) { ?>
							<div id="<?php echo $tab['tab-id']; ?>" class="view"<?php echo $tab['active'] ? '' : ' style="display: none;"'; ?>>
								<?php echo do_shortcode('[' . $tab['shortcode'] . ']'); ?>
							</div><?php
						} ?>
					</div>

					<?php mred_get_projectview_row(); ?>
					<div class="clearboth"></div>

				</article>

			</div><!-- .theme-content -->
		</div><!-- .theme-page-wrapper -->
	</div><!-- #theme-page -->
	
	<?php endwhile;

	$result = ob_get_clean();

	// Save the result
	$can_cache = true;
	$fh = fopen($cache_file, 'w') or $can_cache = false;
	if ($can_cache) {
		fwrite($fh, $result);
		fclose($fh);
	}

	echo $result;
}


$tabs = array(
	array(
		'tab-id'       => 'apartment-images-content',
		'analytics-id' => 'apartment-images',
		'class'        => 'apartment-images',
		'label'        => 'images',
		'shortcode'    => 'lot_list_plan',
		'active'       => true
	),
	array(
		'tab-id'       => 'apartment-list-content',
		'analytics-id' => 'apartment-list',
		'class'        => 'apartment-list',
		'label'        => 'list',
		'shortcode'    => 'lot_list',
		'active'       => false
	)
);

wp_enqueue_style('js_composer_front');

mred_show_page_header();

mredtemplate_get_lot_list($tabs);

get_footer(); ?>