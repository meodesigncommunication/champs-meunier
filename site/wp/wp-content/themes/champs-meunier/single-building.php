<?php
/**
 * Template Name: Floor list
 */

function mredtemplate_get_floor_links($building_floors, $active_floor_index) { ?>
	<ul class="building_floor_links"><?php
		$floor_index = 0;
		foreach ($building_floors as $floor) {
			?><li class="building_floor_link floor_<?php echo $floor["id"]; echo ($floor_index == $active_floor_index ? ' active': ''); ?>"><a class="floor_link" data-floor-id="<?php echo $floor["id"]; ?>" href="#floor_<?php echo $floor["id"]; ?>"><?php echo $floor['code']; ?></a></li><?php
			$floor_index++;
		} ?>
	</ul><?php
}

function mredtemplate_get_image_height_as_pct_of_page_width($building_image) {
	$image_width = $building_image['width'];
	$image_height = $building_image['height'];

	// Work out the padding required to keep the height of the div the same as the image
	// Uses the fact the image is 5/6 of the width, the links 1/6
	$result = $image_height / ($image_width  * 6 / 5) * 100; /* height as a % of the page width */
	$result = floor( $result * 10000 ) / 10000; /* truncate to 4dp */

	// Convert French style numbers to decimals understood by the browser
	$result = str_replace(",", ".", $result);

	return $result;
}

function mredtemplate_building_image($building, $building_floors, $active_floor_index) {
	$building_image = get_field('building_image', $building['id']);
	$image_height_pct = mredtemplate_get_image_height_as_pct_of_page_width($building_image);

	?>
	<div class="building_image_wrapper vc_col-sm-10 wpb_column column_container " style="padding-bottom: <?php echo $image_height_pct; ?>%; ">
		<div class="building_image">
			<?php if (!empty($building_image)) { ?>
				<img class="main_image" src="<?php echo $building_image['url']; ?>" alt="b&acirc;timent" />
			<?php } ?>
			<?php mredtemplate_get_floor_links($building_floors, $active_floor_index); ?>
		</div>
	</div>
<?php }


function mredtemplate_building_floor_links($building, $building_floors, $active_floor_index) {
	$building_image = get_field('building_image', $building['id']);
	$link_height_pct = mredtemplate_get_image_height_as_pct_of_page_width($building_image);

	?>
	<div class="vc_col-sm-2 wpb_column column_container building-floor-links-column" style="padding-bottom: <?php echo $link_height_pct; ?>%; ">
		<div class="building-floor-links-wrapper">
			<?php mredtemplate_get_floor_links($building_floors, $active_floor_index); ?>
		</div>
	</div><?php
}


function mredtemplate_floor_scroll_control($building_id, $floors, $floor_index) { ?>
	<div class="floor_navigation">
		<a class="floor_navigation_arrow floor_navigation_arrow_up" href="#"><span class="mk-font-icons mk-shortcode icon-align-none "><i class="mk-moon-arrow-up-6  mk-size-x-large"></i></span></a>
		<a class="floor_navigation_arrow floor_navigation_arrow_down" href="#"><span class="mk-font-icons mk-shortcode icon-align-none "><i class="mk-moon-arrow-down-6  mk-size-x-large"></i></span></a>
		<div class="floor_name">&nbsp;</div>
	</div><?php
}

function mredtemplate_get_lot_types_for_floors() {
	$lots = mred_get_lots();
	$result = array();
	foreach ($lots as $lot) {
		$floor_id = $lot['floor_id'];
		if (!array_key_exists($floor_id, $result)) {
			$result[$floor_id] = array();
		}
		if (!array_key_exists($lot['type']['id'], $result[$floor_id])) {
			$result[$floor_id][$lot['type']['id']] = 0;
		}
		$result[$floor_id][$lot['type']['id']] += 1;
	}

	return $result;
}

function gefi_get_building_filters($building_id) {
	$buildings = mred_get_buildings();

	$result = '';

	$result .= '<ul class="filter-menu sf-menu">';
	$result .=   '<li class=" has-children">';
	$result .=     '<span class="filter-menu-header">' . mred_translate('Building') . '</span>';
	$result .=     '<ul class="submenu">';
	foreach ($buildings as $building) {
		$result .= '<li' . ( $building['id'] == $building_id ? ' class="active"' : '') . '><a href="' . get_permalink($building['id']) . '">' . $building['name'] . '</a></li>';
	}

	$result .=     '</ul>';
	$result .=   '</li>';
	$result .= '</ul>';

	return $result;
}


/* --------------------------------------------------------------------------------------- */

global $wp_query;

wp_enqueue_style('js_composer_front');

mred_show_page_header();

while ( have_posts() ) : the_post();


$building_id = get_the_ID();
$building_floors = mred_get_floors_for_building($building_id);
$building_floors = array_reverse($building_floors); // Show the higher floors first

$lot_list_page_id = mred_get_page_id_from_slug(PLAN_PAGE_SLUG);
$lot_list_page = get_permalink($lot_list_page_id);

$floor_index = 0;

// Add anchors when we click on the floors
//foreach($building_floors as $k => $floor){
//	echo '<a name="floor_'.$floor['id'].'"></a>';
//}
?>
<script type="text/javascript">
var floor_index = <?php echo $floor_index; ?>;
</script>
<div class="filters-wrapper">
	<div class="filters">
		<div class="filter-responsive-link"><?php _e('Filter by', MREDTEMPLATES_TEXT_DOMAIN); ?></i></div>
		<span class="filter-label"><?php _e('Filter by', MREDTEMPLATES_TEXT_DOMAIN); ?></span>
		<?php echo gefi_get_building_filters($building_id); ?>
	</div>
</div>
<div id="theme-page">
	<div id="mk-page-id-<?php echo $post->ID; ?>" class="theme-page-wrapper mk-main-wrapper full-layout mk-grid vc_row-fluid">
		<div class="theme-content" itemprop="mainContentOfPage">
			<div class="view-switchers">
				<span class="label"><?php _e('Display by', MREDTEMPLATES_TEXT_DOMAIN); ?></span>
				<a href="#" class="view-switcher building in-page active">building</a>
				<a href="<?php echo $lot_list_page; ?>#apartment-images-content" class="view-switcher apartment-images" data-analytics-id="apartment-images">images</a>
				<a href="<?php echo $lot_list_page; ?>#apartment-list-content" class="view-switcher apartment-list" data-analytics-id="apartment-list">list</a>
			</div>
			<div class="page-content"><?php the_content(); ?></div>

			<?php

			echo mred_get_building_floor_selector( $building_id, $building_floors, $floor_index, array(
						'building-wrapper-inner' => array(
							'classes' => array('wpb_row', 'vc_row-fluid', 'mk-fullwidth-false', 'add-padding-0', 'attched-false')
						),
						'sections' => array(
							array(
								'function' => 'mredtemplate_building_image'
							),
							array(
								'function' => 'mredtemplate_building_floor_links'
							)
						)
					) ); ?>

			<div id="content">
				<div id="content-inner">
					<?php echo mred_get_floor_scroller($building_id, $building_floors, $floor_index, array(
						'sections' => array(
							array(
								'function' => 'mred_floor_scroll_container',
								'classes' => array('floor-plan-wrapper')
							),
							array(
								'function' => 'mredtemplate_floor_scroll_control',
								'classes' => array('floor-nav-wrapper')
							)
						)
					)); ?>
				</div>
				<?php mred_get_projectview_row(); ?>
			</div>
		</div><!-- .theme-content -->
	</div><!-- .theme-page-wrapper -->
</div><!-- #theme-page -->

<?php endwhile; ?>

<?php get_footer(); ?>
