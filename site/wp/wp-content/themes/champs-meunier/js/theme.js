(function($){

	function make_imagemaps_responsive() {
		$('img[usemap]').rwdImageMaps();
	}

	function add_floor_png_over_functionality() {
		var isMobile = navigator.userAgent.match(/iPhone|iPad|iPod|Android|BlackBerry|IEMobile/i) ? true : false; // not perfect, but will get the important cases

		if (isMobile) {
			return;
		}

		$('.single-development .image-wrapper area').each(function() {
			$(this).on('mouseover', function() {
			  var overlay_class = $(this).data('over'),
				  $overlay = $('#overlay_' + overlay_class),
				  $label = $('#label_overlay_' + overlay_class);
			  $overlay.attr('src', $overlay.data('over'));
			  $label.show();
			});

			$(this).on('mouseout', function() {
			  var overlay_class = $(this).data('over'),
				  $overlay = $('#overlay_' + overlay_class),
				  $label = $('#label_overlay_' + overlay_class);

			  $overlay.attr('src', $overlay.data('off'));
			  $label.hide();
			});
		});
	}

	// Document ready function
	$(function() {
		make_imagemaps_responsive();
		add_floor_png_over_functionality();
	});

})(jQuery);


jQuery(document).ready(function() {

	/* Home page logo fade in */
	if(jQuery('.logo-meunier-home-footer img').length){
		jQuery( ".logo-meunier-home-footer img" ).fadeIn( 10000 );
	}

	// Get highest value between html tag or window (large screens)
	function getGreatestHeight(){
		var htmlElement = document.querySelector("html");
		var htmlElementHeight = jQuery(htmlElement).height();
		var windowElementHeight = jQuery( window ).height();

		var finalHeight = windowElementHeight;
		if(windowElementHeight < htmlElementHeight) { finalHeight = htmlElementHeight; }
		// alert("Window: "+windowElementHeight+" | Html: "+htmlElementHeight+" | Final: "+finalHeight);
		return finalHeight;
	}
	/**/
	var onResize = function() {
		// .mk-header .mk-header-inner .mk-reponsive-wrap
		var theHeight = 1000; // getGreatestHeight();
		// alert(jQuery(window).height() + "-"+ theHeight);
		//jQuery('#mk-responsive-wrap').css('height', theHeight);
		jQuery('#mk-responsive-wrap').css('max-height', 'none');
		jQuery('#mk-responsive-wrap').css('overflow-y', 'auto');
		//jQuery('#mk-responsive-nav').css('height', theHeight);
		jQuery('#mk-responsive-nav').css('max-height', 'none');
	};
	jQuery(window).resize(onResize);
	jQuery(window).on('orientationchange', onResize);

});
