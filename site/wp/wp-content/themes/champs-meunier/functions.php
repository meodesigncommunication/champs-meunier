<?php


define('MREDTEMPLATES_TEXT_DOMAIN', 'mred-templates');
define('PLAN_PAGE_SLUG', 'plans');

/* Add Sessions */
function meo_init_session() {
	if (!session_id()) session_start();
	
	// Print Pricelist (Not enabled by default)
	if(!isset($_SESSION['pricesEnabled'])) $_SESSION['pricesEnabled'] = false;
}
add_action('init', 'meo_init_session', 1);

/* ===========================================================================
 * Load translations
 * =========================================================================== */

add_action('init', 'mredtemplate_lang');
function mredtemplate_lang() {
	load_theme_textdomain(MREDTEMPLATES_TEXT_DOMAIN, get_stylesheet_directory() . '/mredt-languages');
}


/* ===========================================================================
 * Load custom javascript
 * =========================================================================== */
add_action( 'wp_enqueue_scripts', 'mredtemplate_enqueue_scripts_and_styles', 11);
function mredtemplate_enqueue_scripts_and_styles() {
	wp_register_script('responseivemagemaps', get_stylesheet_directory_uri() . '/js/jquery.rwdImageMaps.js', array('jquery'), 'v1.5');
	wp_register_script('mred-theme', get_stylesheet_directory_uri() . '/js/theme.js', array('responseivemagemaps'), filemtime( get_stylesheet_directory() . '/js/theme.js'));

	wp_enqueue_script('responseivemagemaps');
	wp_enqueue_script('mred-theme');
}

/* -----------------------------------------------------------------------------
 * Add filter menu state to body
 * ----------------------------------------------------------------------------- */
add_filter('body_class', 'gefi_body_classes');
function gefi_body_classes($classes) {
        $classes[] = 'filter-menu-closed';
        return $classes;
}
/* -----------------------------------------------------------------------------
 * Allow use of shortcodes in CF7 forms
 * ----------------------------------------------------------------------------- */

add_filter( 'wpcf7_form_elements', 'do_shortcode' );
/* ===========================================================================
 * Override default MRED functions
 * =========================================================================== */
function mred_get_plan_filter_classes($lot_id) {
	$lot = mred_get_lot($lot_id);
	$floor = mred_get_floor($lot['floor_id']);
	$building = mred_get_building($floor['building_id']);
	$sector = mred_get_sector($building['sector_id']);
	$plan = mred_get_plan($lot['plan_id']);

	$filter_classes = array(
		'building_' . $building['id'],
		'sector_' . $sector['id'],
		'floor_' . strip_tags(str_replace(' ', '_', $floor['ordinal'])),
		'rooms_' . preg_replace('/\D/', '_', $lot['pieces']),
		'entry_' . $lot['entry_id'],
		'lot_type_' . $lot['type']['slug'],
		'balcony_' . ( empty($lot['surface_balcony']) ? '0' : '1' ),
		'availability_' . mred_get_availability_class($lot['availability'])
	);

	return $filter_classes;
}

// Plan filters.
// Options can be added and removed with the plan_list_filter_options filter,
// rather than overwriting the entire function
function mred_get_plan_list_filters() {
	$buildings = mred_get_buildings();
	$sectors = mred_get_sectors();
	$floors = mred_get_floors();
	$lots = mred_get_lots();
	$lot_types = mred_get_lot_types();
	$entries = mred_get_entries();

	$building_options = array();
	foreach ($buildings as $building) {
		$building_options[$building['id']] = $building['code'];
	}

	$sector_options = array();
	foreach ($sectors as $sector) {
		$sector_options[$sector['id']] = $sector['code'];
	}

	$floor_options = array();
	foreach ($floors as $floor) {
		// Selection per floor per buildling
		// $floor_options[$floor['id']] = $floor['ordinal'] . (count($buildings) > 1 ? ' - ' . $buildings[$floor['building_id']]['name'] : '');
		// Selection per floor type (rez, 1st, 2nd etc.)
		$floor_code = strip_tags(str_replace(' ', '_', $floor['ordinal']));
		$floor_options[$floor_code] = $floor['ordinal'];
	}

	$entry_options = array();
	foreach ($entries as $entry) {
		$entry_options[$entry['id']] = $entry['name'];
	}

	$lot_type_options = array();
	foreach ($lot_types as $lot_type) {
		$lot_type_options[$lot_type['slug']] = $lot_type['name'];
	}

	$room_options = array();
	foreach ($lots as $lot) {
		$escaped_rooms = preg_replace('/\D/', '_', $lot['pieces']);
		$room_options[$escaped_rooms] = $lot['pieces'] . ' ' . mred_translate('p.');
	}
	asort($room_options);

	$filter_menu_options = array(
		'all' => array(
			'name' => mred_translate('All'),
			'filter_type' => 'all',
			'classes' => 'chosen'
		),
		'lot_types' => array(
			'name' => mred_translate('Type'),
			'filter_type' => 'lot_type',
			'options' => $lot_type_options
		),
		'sector' => array(
			'name' => mred_translate('sector'),
			'filter_type' => 'sector',
			'options' => $sector_options
		),
		'building' => array(
			'name' => mred_translate('Building'),
			'filter_type' => 'building',
			'options' => $building_options
		),
		'floor' => array(
			'name' => mred_translate('Floor'),
			'filter_type' => 'floor',
			'options' => $floor_options
		),
		'entry' => array(
			'name' => mred_translate('Entry'),
			'filter_type' => 'entry',
			'options' => $entry_options
		),
		'rooms' => array(
			'name' => mred_translate('Rooms'),
			'filter_type' => 'rooms',
			'options' => $room_options
		),
		'balcony' => array(
			'name' => mred_translate('Balcony'),
			'filter_type' => 'balcony',
			'options' => array(
				'0' => mred_translate('No'),
				'1' => mred_translate('Yes')
			)
		),
		'availability' => array(
			'name' => mred_translate('Availability'),
			'filter_type' => 'availability',
			'options' => mred_get_availability_descriptions()
		)
	);

	$filter_menu_options = apply_filters('plan_list_filter_options', $filter_menu_options);


	$result = '';

	$result .= '<ul class="filter-menu sf-menu">';

	foreach ($filter_menu_options as $filter_menu_option) {
		$name = $filter_menu_option['name'];
		$filter_type = $filter_menu_option['filter_type'];
		$classes = empty($filter_menu_option['classes']) ? '' : $filter_menu_option['classes'];
		$options = $filter_menu_option['options'];
		$has_children = !empty($options);
		if ($has_children) {
			$classes .= ' has-children';
		}

		$result .= '<li' . ( $classes ? ' class="' . $classes . '"' : '' ) . '>';
		if (!$has_children) {
			$result .= '<a data-filter-type="' . $filter_type . '" href="#' . $filter_type . '">';
		}
		$result .= '<span class="filter-menu-header">' . $name . '</span>';
		if ($has_children) {
			$result .= '<ul class="submenu">';
			foreach ($options as $option_value => $option_name) {
				$result .= '<li><a data-filter-type="' . $filter_type . '" data-filter-value="' . $option_value . '" href="#' . $filter_type . '_' . $option_value . '">' . $option_name . '</a></li>';
			}
			$result .= '</ul>';
		}

		if (!$has_children) {
			$result .= '</a>';
		}
		$result .= '</li>';

	}

	$result .= '</ul>'; // .filter-menu

	return $result;
}

function mred_get_lot_plan_classes() {
	return array('vc_col-sm-3', 'wpb_column', 'column_container');
}

function mred_get_lot_markup_list ($lot, $floor_link, $classes = array()) {
	
	global $download_lot_id;
	$download_lot_id = $lot['id'];
	
	$floor = mred_get_floor($lot['floor_id']);
	$building = mred_get_building($floor['building_id']);
	$lot_type = empty($lot['type']) ? '' : $lot['type']['slug'];
	$lot_link = get_permalink($lot['id']);
	if (function_exists('qtrans_convertURL')) {
		$lot_link = qtrans_convertURL($lot_link, $current_language);
	}
	$filter_classes = mred_get_plan_filter_classes($lot['id']);

	// <a href="<?php echo $floor_link; ? >#floor_<?php echo $lot['floor_id']; ? >">	
	ob_start();
	
	// Availability / Price
	$availabiltyPrice = mred_get_availability_description($lot['availability']);
	
	if(isset($_SESSION['pricesEnabled']) && $_SESSION['pricesEnabled'] === true && $lot['availability'] == 'available'){
		if(isset($lot['price']) && $lot['price'] != '') {
			$availabiltyPrice = number_format((int)$lot['price'], 0, ".", "'");
			$availabiltyPrice = $availabiltyPrice.'CHF';
		}
	}
	?>

	<li class="apartment-list-row <?php echo join(" ", $classes); ?> <?php echo join(" ", mred_get_lot_list_classes()); ?> <?php echo join(" ", $filter_classes); ?>">
		<ul>
			<li class="apartment_code_and_availability">
				<a href="<?php echo $lot_link; ?>">
					<div class="apartment_code"><?php echo $lot['code']; ?></div>
					<div class="availability"><?php echo $availabiltyPrice; ?></div>
				</a>
			</li>
			<li class="building"><a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>"><span class="list_icon list_icon_building"></span><span class="list_building_text"><?php echo $building['name']; ?></span></a></li>
			<li class="floor"><a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>"><span class="list_icon list_icon_floor"></span><span class="list_building_text"><?php echo $floor['ordinal']; ?></span></a></li>
			<li class="rooms"><a href="<?php echo $lot_link; ?>"><span class="list_icon list_icon_rooms"></span><span class="list_building_text"><?php echo $lot['pieces']; echo '&nbsp;<span class="lower">' . mred_translate('p.') . '</span>'; ?></span></a></li>
			<li class="area"><a href="<?php echo $lot_link; ?>"><span class="list_icon list_icon_area"></span><span class="list_building_text"><?php echo $lot['surface_weighted']; echo '&nbsp;<span class="lower">m</span><sup>2</sup>';  ?></span></a></li>
			<li class="pdf-download"><a class="fancybox-iframe pdf-download-link" href="<?php echo mred_get_pdf_request_url($lot['id']); ?>"><?php echo mred_translate('Info pack'); ?></a></li>
		</ul>
	</li><?php

	return apply_filters( 'mred_get_lot_markup_list', ob_get_clean() );
}

// Return a list item containing the HTML for plan portion of the list page
// Theme-specific classes (eg column classes) can be passed
if (!function_exists( 'mred_get_lot_markup_plan')) {
	function mred_get_lot_markup_plan ($lot, $floor_link, $classes = array()) {
		$floor = mred_get_floor($lot['floor_id']);
		$building = mred_get_building($floor['building_id']);
		$plan = mred_get_plan($lot['plan_id']);

		$lot_link = get_permalink($lot['id']);
		if (function_exists('qtrans_convertURL')) {
			$lot_link = qtrans_convertURL($lot_link, $current_language);
		}

		$filter_classes = mred_get_plan_filter_classes($lot['id']);

		$image = get_stylesheet_directory_uri() . '/images/transparent.gif';

		if ($plan['detailed_plan_image']) {
			 $image_details = wp_get_attachment_image_src($plan['detailed_plan_image']['id'], 'plan-small');
			 $image = $image_details[0];
		}

		if ($plan['detailed_plan_image_fallback']) {
			 $image_details = wp_get_attachment_image_src($plan['detailed_plan_image_fallback']['id'], 'plan-small');
			 $fallback_image = $image_details[0];
		}
		
	
		// Availability / Price
		$availabiltyPrice = mred_get_availability_description($lot['availability']);
		
		if(isset($_SESSION['pricesEnabled']) && $_SESSION['pricesEnabled'] === true && $lot['availability'] == 'available'){
			if(isset($lot['price']) && $lot['price'] != '') {
				$availabiltyPrice = number_format((int)$lot['price'], 0, ".", "'");
				$availabiltyPrice = $availabiltyPrice.'CHF';
			}
		}
		
		ob_start();
		?>

		<li class="apartment-list-image-wrapper <?php echo join(" ", $classes); ?> <?php echo join(" ", mred_get_lot_plan_classes()); ?> <?php echo join(" ", $filter_classes); ?>">
			<div class="apartment-list-image">
				<div class="apartment_code"><a href="<?php echo $lot_link; ?>"><span class="lot_type"><?php echo $lot['type']['name']; ?> </span><?php echo $lot['code']; ?></a></div>
				<div class="apartment_details">
				
				<a href="<?php echo $building['url']; ?>">
						<span class="floor"><?php echo $building['name']; ?> <span class="separator">|</span></span>
					</a>
					
				<a href="<?php echo $floor_link; ?>#floor_<?php echo $lot['floor_id']; ?>">
						<span class="floor"><?php echo $floor['ordinal']; ?> <span class="separator">|</span></span>
					</a>
					
					<a href="<?php echo $lot_link; ?>">
						<span class="rooms"><?php echo $lot['pieces']; echo '&nbsp;<span class="lower">' . mred_translate('p.') . '</span>'; ?> <span class="separator">|</span></span>
					</a>
					
					<a href="<?php echo $lot_link; ?>">
						<span class="area"><?php echo $lot['surface_weighted']; echo '&nbsp;<span class="lower">m</span><sup>2</sup>';  ?></span>
					</a>
				</div>
				<div class="availability-wrapper"><a class="availability" href="<?php echo $lot_link; ?>"><?php echo $availabiltyPrice; ?></a></div>
				<div class="plan"><a href="<?php echo $lot_link; ?>"><img src="<?php echo $image; ?>" data-fallback="<?php echo $fallback_image; ?>" alt="<?php echo $lot['code']; ?>"></a></div>
				<div class="pdf-download"><?php echo mred_get_download_button($lot['id']); ?></div>
			</div>
			<a href="<?php echo $lot_link; ?>" class="apartment-list-overlay" style="display: none;">&nbsp;</a>
		</li>
		<?php

		return apply_filters( 'mred_get_lot_markup_plan', ob_get_clean() );
	}
}

function mred_get_lot_breadcrumb($lot_id) {
	$lot = mred_get_lot($lot_id);
	$floor = mred_get_floor($lot['floor_id']);
	$building = mred_get_building($floor['building_id']);
	$sector = mred_get_sector($building['sector_id']);
	$development = mred_get_development($sector['development_id']);
	
	$list_page_id   =  mred_get_page_id_from_slug( 'plans-par-liste');
	$list_page_link = get_permalink($list_page_id);
	
	$links = array(
		array(
			'title' => 'Plan de situation',
			'url'   => $development['url']
		),
		array(
			'title' => $building['name'],
			'url'   => $building['url'], 
		),
		array(
			'title' => $lot['name'],
			'url'   => '#',
		)
	);

	$result = '<div class="breadcrumb">';
	
	$i = 1;
	$max  = count($links);
	foreach ($links as $link) {
		$result .= '<a href="' . $link['url'] .'">' . $link['title'] . '</a>';
		if($i < $max) {
			$result .= ' &gt; ';
		}
		$i++;
	}

	$result .= '</div>';

	return $result;
}

add_filter('mred_get_lot_markup_plan', 'gefi_get_lot_markup_plan');
function gefi_get_lot_markup_plan($element) {
	// Add an extra div into the thumbnails
	$result = preg_replace('/^\s*(<li[^>]*>)(.*)(<\/li>)\s*$/s', '$1<div class="apartment-list-image-inner">$2</div>$3', $element);

	// Add the underline after the lot header
	$separator = do_shortcode('[vc_separator color="grey" align="align_center" el_width="50"]');
	$result = preg_replace('/(<div class="apartment_code"><a href="[^"]+"><span class="lot_type">[^<]*<\/span>[^<]*<\/a><\/div>)/s', '$1' . $separator, $result);

	return $result;
}

add_filter('plan_list_filter_options', 'gefi_plan_list_filter_options');
function gefi_plan_list_filter_options($filter_menu_options) {
	foreach (array('sector', 'entry', 'lot_types', 'balcony') as $index) {
		unset($filter_menu_options[$index]);
	}

	$buildings = mred_get_buildings();

	$building_options = array();
	foreach ($buildings as $building) {
		$building_options[$building['id']] = $building['name'];
	}

	$filter_menu_options['building']['options'] = $building_options;

	return $filter_menu_options;
}


/* ===========================================================================
 * Configure Advanced Custom Fields plugin
 * =========================================================================== */

define('ACF_OPTION_PAGE_STUB', 'acf-options-champs-meunier-settings');

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page('Champs Meunier Settings'); // Note - if changing this, the ACF_OPTION_PAGE_STUB will need to change
}

if( function_exists('register_field_group') ):

register_field_group(array (
	'key' => 'group_54dccd889a11f',
	'title' => 'Champs Meunier Settings',
	'fields' => array (
		array (
			'key' => 'field_54dccdaa6f49f',
			'label' => 'Logo',
			'name' => 'logo',
			'prefix' => '',
			'type' => 'image',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'return_format' => 'array',
			'preview_size' => 'medium',
			'library' => 'all',
		),
		array (
			'key' => 'field_54dccddc6f4a0',
			'label' => 'Adresse',
			'name' => 'address',
			'prefix' => '',
			'type' => 'textarea',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'maxlength' => '',
			'rows' => 4,
			'new_lines' => 'br',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_54dcce096f4a1',
			'label' => 'Phone',
			'name' => 'phone',
			'prefix' => '',
			'type' => 'text',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
		array (
			'key' => 'field_54dcce126f4a2',
			'label' => 'email',
			'name' => 'email',
			'prefix' => '',
			'type' => 'email',
			'instructions' => '',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
		),
		array (
			'key' => 'field_54dcce1b6f4a3',
			'label' => 'Adresse web',
			'name' => 'url',
			'prefix' => '',
			'type' => 'text',
			'instructions' => 'Without http://',
			'required' => 0,
			'conditional_logic' => 0,
			'wrapper' => array (
				'width' => '',
				'class' => '',
				'id' => '',
			),
			'default_value' => '',
			'placeholder' => '',
			'prepend' => '',
			'append' => '',
			'maxlength' => '',
			'readonly' => 0,
			'disabled' => 0,
		),
	),
	'location' => array (
		array (
			array (
				'param' => 'options_page',
				'operator' => '==',
				'value' => ACF_OPTION_PAGE_STUB,
			),
		),
	),
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
));

endif;


/* ===========================================================================
 * Utility functions
 * =========================================================================== */

function gefi_encode_email( $email, $label = "" ) {
  if (!is_email($email)) {
    return $email;
  }

  if (empty($label)) {
    $label = $email;
  }

  $ascii_label = unpack('C*', $label);
  $ascii_email = unpack('C*', $email);

  $mailto = "&#109;a&#105;l&#116;&#111;:"; // encoded "mailto:" string

  $encoded_label = "&#" . join(";&#", $ascii_label) . ";";
  $encoded_email = "&#" . join(";&#", $ascii_email) . ";";

  return '<a href="' . $mailto . $encoded_email . '">' . $encoded_label . '</a>';
}
/* Call To Action [ call_to_action_content | call_to_action_url | call_to_action_enabled ] */
function gefi_get_active_call2action($post_id){

	$calls_to_action = get_field('call_to_action', $post_id);
	$calls = count($calls_to_action);
	// To collect only activated calls, if any
	$active_calls = array();
	$x = 0;

	if(isset($calls_to_action) && $calls > 0 && !empty($calls_to_action)) {
		foreach ($calls_to_action as $call => $action) {
			if($action['call_to_action_enabled'] == 1) {
				$active_calls[$x]['content'] = $action['call_to_action_content'];
				$active_calls[$x]['url'] = '';
				$active_calls[$x]['url_text'] = '';
				// Something to link to
				/* Text field */
				if(!empty($action['call_to_action_url'])){
					$active_calls[$x]['url'] = $action['call_to_action_url'];
					$active_calls[$x]['url_text'] = $action['call_to_action_url_text'];
				}
				/* Change for this code, if they finally want the file field again
				if(is_array($action['call_to_action_url']) && array_key_exists('url', $action['call_to_action_url'])){
					$active_calls[$x]['url'] = $action['call_to_action_url']['url'];
					$active_calls[$x]['url_text'] = $action['call_to_action_url_text'];
				}
				*/
				$x++;
			}
		}
  	}

  	if($x > 0) {
  		$i = rand(0, $x - 1);
  		return $active_calls[$i];
  	}
  	else return false;
}
/* Shortcode to generate located CalltoAction buttons [call2actionbtn title | url | target | class | linkclass ]
 * [call2actionbtn title="" url="" target="" class=""] */
  	function call2action_shortcode( $atts ) {
    $a = shortcode_atts( array(
        'title' => '',
        'url' => '#',
    	'target' => '_self',
    	'class' => '',
    	'linkclass' => '',
    ), $atts );

    return '<div class="c2a-link-single '.$a['class'].'"><a href="'.$a['url'].'" target="'.$a['target'].'" class="'.$a['linkclass'].'">'.$a['title'].'</a></div>';
}
add_shortcode( 'call2actionbtn', 'call2action_shortcode' );

/**
 * This function will connect wp_mail to your authenticated
 * SMTP server. This improves reliability of wp_mail, and 
 * avoids many potential problems.
 *
 * Author:     Chad Butler
 * Author URI: http://butlerblog.com
 *
 * For more information and instructions, see:
 * http://b.utler.co/Y3
 */
add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {

	// Define that we are sending with SMTP
	$phpmailer->isSMTP();

	// The hostname of the mail server
	$phpmailer->Host = "mail.infomaniak.ch";

	// Use SMTP authentication (true|false)
	$phpmailer->SMTPAuth = true;

	// SMTP port number - likely to be 25, 465 or 587
	$phpmailer->Port = "587";

	// Username to use for SMTP authentication
	$phpmailer->Username = "webmaster@champs-meunier.ch";

	// Password to use for SMTP authentication
	$phpmailer->Password = "w3bM3Un!mail!";

	// Encryption system to use - ssl or tls
	$phpmailer->SMTPSecure = "tls";

	$phpmailer->From = "no-reply@champs-meunier.ch";
	$phpmailer->FromName = "Champs Meunier";
	
	// print_r($phpmailer);
}

/* Emulate Localize to Cache the JS content from Db (from mred_localise() MEO Real Estate Plugin) */

#######################################################################################
## /!\ Comment add_action( 'wp_enqueue_scripts', 'mred_localise' );              /!\ ##
## /!\ in meo-real-estate-developments.php in meo-real-estate-development plugin /!\ ##
#######################################################################################

/**
 * meo_dataToJS
 * @param $varName (string): Javascript var name
 * @param $l10n (string): PHP code to convert
 * @param $fullscriptEmbed (bool) : boolean to add or not the javascript tags
 * /
function meo_dataToJS( $varName, $l10n, $fullscriptEmbed = false ) {

	// back compat, preserve the code in 'l10n_print_after' if present
	if ( is_array($l10n) && isset($l10n['l10n_print_after']) ) { 
		$after = $l10n['l10n_print_after'];
		unset($l10n['l10n_print_after']);
	}

	foreach ( (array) $l10n as $key => $value ) {
		if ( !is_scalar($value) )
			continue;

		$l10n[$key] = html_entity_decode( (string) $value, ENT_QUOTES, 'UTF-8');
	}
	
	// Return
	$return = '';
	
	if($fullscriptEmbed){$return .= '<script type="text/javascript">/* <![CDATA[ * /'."\n";}
	
	$script = "var $varName = " . wp_json_encode( $l10n ) . ';'."\n";
	$return .= $script;
	
	if($fullscriptEmbed){$return .= '/* ]]> * /</script>';}

	return $return;
}
/* Add Btn to Generate Manually the script file * /
/**
 * This allows to avoid to query multiple times, per page, the database
 * The file contains the generated code once, and can be updated manually
 * in the admin section
 * 
 * Btn is in the admin footer
 * Script file is under the wp-content/uploads/cache folder
 * /
add_action('admin_footer', 'generate_script_button');
function generate_script_button() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {

		if(jQuery('#generate-script').length){
			var gs = jQuery('#generate-script');
			gs.click(function(e){
				e.preventDefault();
				var data = {
					'action': 'generate_script'
				};
		
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				$.post(ajaxurl, data, function(response) {
					alert('Resultat: ' + response);
				});
			});
		}
	});
	</script>
	<style>
	<!--
	#script-generation {
	  background: none repeat scroll 0 0 #f7a770;
	  bottom: 0;
	  clear: both;
	  display: block;
	  height: 30px;
	  padding: 5px 0 15px;
	  position: absolute;
	  right: 0;
	  text-align: center;
	  width: 400px;
	}
	#script-generation a {color:#fff;font-weight: bold;}
	-->
	</style>
	<div id='script-generation'>
		<p>
			<a href='#' class='sg' id='generate-script'>R&eacute;g&eacute;n&eacute;rer le fichier Informations (B&acirc;timent, Etage, Lot...)</a>
		</p>
	</div>
	<?php 
}
add_action( 'wp_ajax_generate_script', 'generate_script_callback' );

function generate_script_callback($echo = false) {
	global $wpdb; // this is how you get access to the database
	
	// Get all the info from Database
	$dataToJS = array(
		'lots' => mred_get_lots(array('pdf', 'floor_position_image', 'floor_position_image_fallback', 'lot_situation_image', 'lot_situation_image_fallback', 'price')),
		'plans' => mred_get_plans(array('pdf')),
		'floors' => mred_get_floors(array('plan')),
		'sectors' => mred_get_sectors(),
		'buildings' => mred_get_buildings(),
		'lot_types' => mred_get_lot_types(),
		'entries' => mred_get_entries(),
		'translations' => array(
			'rooms' => mred_translate('Rooms'),
			'apartment' => mred_translate('Apartment'),
			'statuses' => mred_get_availability_descriptions()
		)
	);
	// Turn Data into JS code
	$script = meo_dataToJS('mred', $dataToJS, true);
	// Get Script file path
	$cachedJSfileContent2 = dirname(__FILE__).'/../../uploads/cache/';
	$filePath = realpath($cachedJSfileContent2);
	$filePath.= '/script.txt';
	
	// Fill script file
	$cached = fopen($filePath, 'w');
	fwrite($cached, $script);
	fclose($cached);
	
	// Echo the content (from header.php if file didn't exist (now it's created btw))
	if($echo){
		echo $script;
		return true;
	}
	else {
		echo 'Ficher cree | File created | Content cached';
		wp_die();
	}
}
 */