<?php
/**
 * Single Development Template
 */

add_filter( 'wp_title', 'mredtemplate_dev_page_title', 10, 2 );
function mredtemplate_dev_page_title( $title, $sep ) {
	return $sep . 'plan masse';
}

mred_show_page_header();

$development_id = get_the_ID();

$plan = get_field('plan', $development_id);
$plan_hover_transparent = get_field('plan_hover_transparent', $development_id);

$buildings = mred_get_buildings();

//print_r($buildings);

$imagemappolygons = array();
foreach ($buildings as $building) {
	// Sector > Dev > Buildings
	if ($building['sector_id']) {
		$sector = mred_get_sector($building['sector_id']);
		if($sector['development_id'] != $development_id)
		continue;
	}
	/*
	if ($building['development_id'] != $development_id or empty($building['dev_plan_coordinates'])) {
		continue;
	}
	*/
	$imagemappolygons[] = array(
		'name' => $building['name'],
		'slug' => $building['slug'],
		'coordinates' => $building['dev_plan_coordinates'],
		'url' => get_permalink($building['id']),
		'hover-image' => $building['dev_plan_hover_image']['url']
	);
}

$floors = mred_get_floors();
foreach ($floors as $floor) {
	$building = mred_get_building($floor['building_id']);
	if ($building['development_id'] != $development_id or empty($floor['dev_plan_coordinates'])) {
		continue;
	}
	$imagemappolygons[] = array(
		'name' => $building['name'],
		'slug' => $floor['slug'],
		'coordinates' => $floor['dev_plan_coordinates'],
		'url' => get_permalink($building['id']) . '#floor_' . $floor['id'],
		'hover-image' => $floor['dev_plan_hover_image']['url']
	);
}
$usemap = !empty($imagemappolygons);

?>
<div id="theme-page">
	<div class="theme-page-wrapper full-layout vc_row-fluid mk-grid row-fluid">
		<div class="theme-content">
			<?php the_content(); ?>
			<div class="image-wrapper-outer">
				<div class="image-wrapper">
					<img class="plan" src="<?php echo $plan['url']; ?>" width="<?php echo $plan['width']; ?>" height="<?php echo $plan['height']; ?>" alt="<?php the_title(); ?>" />
					<img class="plan-overlay-transparent" src="<?php echo $plan_hover_transparent['url']; ?>" width="<?php echo $plan_hover_transparent['width']; ?>" height="<?php echo $plan_hover_transparent['height']; ?>" alt="<?php the_title(); ?>"<?php echo $usemap ? ' usemap="#buildings"': ''; ?> />

					<?php if ($usemap) { ?>
						<map id="buildings" name="buildings">
							<?php foreach ($imagemappolygons as $imagemappolygon) { ?>
								<area href="<?php echo $imagemappolygon['url']; ?>" shape="polygon" coords="<?php echo $imagemappolygon['coordinates']; ?>" alt="<?php echo $imagemappolygon['name']; ?>" data-over="<?php echo $imagemappolygon['slug']; ?>">
							<?php } ?>
						</map>
					<?php } ?>
					<?php foreach ($imagemappolygons as $imagemappolygon) { ?>
						<img id="overlay_<?php echo $imagemappolygon['slug']; ?>" class="plan_overlay" src="<?php echo $plan_hover_transparent['url']; ?>" data-off="<?php echo $plan_hover_transparent['url']; ?>" data-over="<?php echo $imagemappolygon['hover-image']; ?>">
					<?php } ?>
				</div>
			</div>
			<div class="clearboth"></div>
		</div>

	<div class="clearboth"></div>
	</div>
</div>
<?php get_footer(); ?>